require('dotenv').config()
const express = require('express');
const mongoose = require('mongoose');
const axios = require('axios');


const cookieParser = require("cookie-parser");
const router = require('./src/router/index')
const errorMiddleware = require('./src/middlewares/error-middleware')


const PORT = process.env.PORT || 5000

const app = express();
app.use(express.json());
app.use(cookieParser());

app.use('/api', router);


//НА УДАЛЕНИЕ
const url = `https://client.survstat.ru/Home/GetSurveySandboxView/?token=6PqvM00RzrgrJPykr1QNKopRJy1evz2VTcw+KkOZTUBXQal1ztL2i5DEPV//bjgEkzztePyztJDXyhV8WT1sbzAi2v5p22uNC24wXRxsjs29RBNnPl9FugrdQlp85ow703uWPStGi06bNR5V82kQrSyEQU/9C14QyWXw1KYVWZqdzj1j8S1hZ6qceC/KrnMvkz88gWhEOKFrNI2sxfyUGtw2/02rsZXhWTrnqDFckOs=&questions=q1%2Cq2&status=18&interviewPage=0&endDate=0&showQuestions=1&rawHTML=1`
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*'); // Здесь можно установить определенный источник
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});
app.get('/api/vit/data', async (req, res) => {
  try {
    const response = await axios.get(url, {
      headers: {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36'
      }
    });
    res.json(response.data);
  } catch (error) {
    console.log(error)
    res.status(500).json({ error });
  }
});
//НА УДАЛЕНИЕ

app.use(errorMiddleware)


const start = async () => {
  try {
    const user = process.env.DB_USER_NAME
    const pwd = process.env.DB_USER_PASS

    let uri = "mongodb://"
    if (user && pwd) {
      uri += `${user}:${pwd}@`
    }
    uri += "127.0.0.1:27017/TeamWatcher"

    await mongoose.connect(uri, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    })
    console.log('Connection to the database has been completed')

    app.listen(PORT, () => {
      console.log('Server is running on port ' + PORT);
    })
  }catch (e) {
    console.log('Start Error: ' + e)
  }
}

start()



