const {DepService} = require("../service/structure/dep-service");

class DepController {
  async getAll(req, res) {
    const {orgId, depId, isService} = req

    const data = await DepService.getAll(orgId, depId, isService)

    res.json(data)
  }

  async create(req, res) {
    const {title, ownerId, parentId, displayTitle} = req.body
    const {orgId, user} = req

    const data = await DepService.create(orgId, user.id, {
      title,
      ownerId,
      parentId,
      displayTitle
    })

    res.json(data)
  }

  async update(req, res) {
    const id = req.params.id
    const {title, ownerId} = req.body
    const {orgId, user} = req

    const org = await DepService.update(orgId, id, user.id, title, ownerId)

    res.json(org)
  }

  async delete(req, res) {
    const id = req.params.id
    const {orgId} = req

    const data = await DepService.delete(orgId, id)

    res.json(data)
  }
}

module.exports.DepController = new DepController()