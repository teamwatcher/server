const authService = require('../service/auth-service')
const {OrgService} = require("../service/structure/org-service");
const mongoose = require("mongoose");
const {DepService} = require("../service/structure/dep-service");

class AuthController {
  async registration(req, res) {
    const {email, password, fullName, org} = req.body

    const userId = new mongoose.Types.ObjectId()
    let depId = new mongoose.Types.ObjectId()
    if (org.isNew) {
      const orgModel = await OrgService.create(org.title, userId, depId)
      org.id = orgModel.id

      await DepService.create(org.id, userId, {
        _id: depId,
        ownerId: userId,
        title: org.title,
        root: true
      })
    }else {
      const {rootDep} = await OrgService.findById(org.id)
      depId = rootDep.id
    }

    const userData = await authService.registration(userId, email, password, fullName, org.id, depId)

    AuthController.setRefreshTokenCookie(res, userData.refreshToken)
    return res.json(userData)
  }

  async preregistration(req, res) {
    const {email, user} = req.body;
    const {orgId} = req

    const userData = await authService.preregistration(orgId, email, user)

    return res.json(userData)
  }

  async login(req, res) {
    const {email, password} = req.body
    const userData = await authService.login(email, password)
    AuthController.setRefreshTokenCookie(res, userData.refreshToken)
    return res.json(userData)
  }


  static setRefreshTokenCookie(res, refreshToken) {
    res.cookie('refreshToken', refreshToken, {maxAge: 60 * 24 * 60 * 60 * 1000, httpOnly: true})
  }

  async reset(req, res) {
    const {email} = req.body
    await authService.reset(email)

    return res.json()
  }

  async logout(req, res) {
    const {refreshToken} = req.cookies
    const token = await authService.logout(refreshToken)
    res.clearCookie('refreshToken')
    res.json(token)
  }

  async activate(req, res) {
    const activationLink = req.params.link
    await authService.activate(activationLink)

    return res.redirect(process.env.CLIENT_URL)
  }

  async invitation(req, res) {
    const invitationLink = req.params.link
    const code = await authService.invitation(invitationLink)

    return res.redirect(`${process.env.CLIENT_URL}/register/link?c=${code}`)
  }

  async invitationComplete(req, res) {
    const {password, code} = req.body
    const userData = await authService.invitationComplete(code, password)

    AuthController.setRefreshTokenCookie(res, userData.refreshToken)
    return res.json(userData)
  }

  async refresh(req, res) {
    const {refreshToken} = req.cookies

    const userData = await authService.refresh(refreshToken)

    AuthController.setRefreshTokenCookie(res, userData.refreshToken)
    return res.json(userData)
  }
}

module.exports = new AuthController()