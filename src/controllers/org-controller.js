const {OrgService} = require("../service/structure/org-service");

class OrgController {
  async update(req, res) {
    const {title, permission} = req.body
    const {orgId, user} = req

    const org = await OrgService.update(orgId, user.id, {title, permission})

    res.json(org)
  }

  async bind(req, res) {
    const {orgId, userId} = req.body

    res.json(true)
  }

  async getById(req, res) {
    const id = req.params.id

    const org = await OrgService.findById(id)

    return res.json(org)
  }
}

module.exports.OrgController = new OrgController()