const {FileService} = require("../service/file-service");
const {UserService} = require('../service/structure/user-service')

class FileController {
  static downloadStatic(res, filePath) {
    res.download(filePath, (err)=>{
      res.json(err)
    });
  }

  static async downloadBuffer(res, fileName, fileType, buffer) {
    res.set('Content-Disposition', `attachment; filename="${fileName}.${fileType}"`);
    res.send(buffer);
  }

  static async parseUsersExcel(req, res) {
    const {orgId, user, file} = req
    let [header, ...data] = await FileService.parseXLSX(file)
   const response = await UserService.parseXLSXData(header, data)

    res.json(response)
  }

  static async downloadTemplateImport(req, res) {
    const filePath = './storage/TemplateImportUsers.xlsx';

    FileController.downloadStatic(res, filePath)
  }
}

module.exports.fileController = FileController