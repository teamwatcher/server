const taskService = require('../service/task-service')

class TaskController {
  async create(req, res) {
    const {title, depId} = req.body
    const {orgId, user} = req

    const task = await taskService.create(orgId, user.id, depId, {title})

    res.json(task)
  }

  async getAll(req, res) {
    const {orgId, depId, isService} = req

    const tasks = await taskService.getAll(orgId, depId, isService)

    res.json(tasks)
  }

  async update(req, res) {
    const id = req.params.id
    const {title} = req.body
    const {orgId, user} = req

    const task = await taskService.update(orgId, user.id, id, {title})

    res.json(task)
  }

  async delete(req, res) {
    const id = req.params.id
    const {orgId} = req

    const task = await taskService.delete(orgId, id)

    res.json(task)
  }
}

module.exports = new TaskController()