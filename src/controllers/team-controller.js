const teamService = require('../service/team-service')

class TeamController {
  async create(req, res) {
    const {title, parentId, ownerId, depId} = req.body
    const {orgId, user} = req

    const team = await teamService.create(orgId, depId, user.id, {title, parentId, ownerId})

    res.json(team)
  }

  async update(req, res) {
    const id = req.params.id
    const {title, parentId, ownerId} = req.body
    const {orgId, user} = req

    const team = await teamService.update(orgId, user.id, id, {title, parentId, ownerId})

    res.json(team)
  }

  async delete(req, res) {
    const id = req.params.id
    const {orgId} = req

    const team = await teamService.delete(orgId, id)

    res.json(team)
  }

  async getAll(req, res) {
    const {orgId, depId, isService} = req

    const teams = await teamService.getAll(orgId, depId, isService)

    res.json(teams)
  }
}

module.exports = new TeamController()