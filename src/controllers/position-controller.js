const positionService = require('../service/position-service')

class PositionController {
  async create(req, res) {
    const {title, displayTitle, depId} = req.body
    const {orgId, user} = req

    const position = await positionService.create(orgId, user.id, {title, displayTitle, depId})

    res.json(position)
  }

  async update(req, res) {
    const id = req.params.id
    const {title, displayTitle} = req.body
    const {orgId, user} = req

    const position = await positionService.update(orgId, user.id, id, title, displayTitle)

    res.json(position)
  }

  async delete(req, res) {
    const id = req.params.id
    const {orgId} = req

    const position = await positionService.delete(orgId, id)

    res.json(position)
  }


  async getAll(req, res) {
    const {orgId, depId, isService} = req

    const positions = await positionService.getAll(orgId, depId, isService)

    res.json(positions)
  }
}

module.exports = new PositionController()