const tokenService = require('../service/token-service')
const {deletePlannerUserData, updatePlannerUserData} = require("../_modules/planner");
const {deletePerformanceUserData} = require("../_modules/performance");
const {STATUSES} = require("../constants/user-constants");
const uuid = require("uuid");
const {fileController} = require("./file-controller");
const {UserService} = require("../service/structure/user-service");

class UserController {
  async getAll(req, res) {
    const {orgId, depId, isService} = req

    const users = await UserService.getAllUsers(orgId, depId, isService)

    await updatePlannerUserData(orgId, users)

    return res.json(users)
  }

  async getById(req, res) {
    const {orgId} = req
    const {userId} = req.params

    const user = await UserService.getById(orgId, userId)

    return res.json(user)
  }

  async update(req, res) {
    const {userId} = req.params
    const {user} = req.body
    const {orgId} = req

    const data = await UserService.update(orgId, user.id, userId, user)

    res.json(data)
  }

  async delete(req, res) {
    const {orgId, user} = req
    const {userId } = req.params

    const data = await UserService.delete(orgId, userId)

    await deletePlannerUserData(orgId, userId)
    await deletePerformanceUserData(orgId, userId)
    await tokenService.deleteByUser(userId)

    res.json(data)
  }

  async invite(req, res) {
    const {orgId, user} = req
    const {userId} = req.params
    const invitationLink = uuid.v4()
    const data = await UserService.update(orgId, user.id, userId, {
      status: STATUSES.INVITED,
      invitationLink,
      meta: {
        invitedAt: Date.now(),
        invitedBy: user.id
      }
    })

    await UserService.invite(data.email, invitationLink, user.displayName, data.displayName)

    res.json(data)
  }

  async export(req, res) {
    const {orgId, user} = req

    const buffer = await UserService.exportUsers(orgId)
    fileController.downloadBuffer(res, 'User List', 'xlsx', buffer)
  }

  async taskAdd(req, res) {
    const {orgId} = req
    const {userId} = req.params
    const {id} = req.body

    const data = await UserService.TASK.add(orgId, userId, id)

    res.json(data)
  }

  async taskRemove(req, res) {
    const {orgId} = req
    const {userId, id} = req.params

    const data = await UserService.TASK.remove(orgId, userId, id)

    res.json(data)
  }
}

module.exports = new UserController()