const monthFullLabels = ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря',]

Date.prototype.withoutTime = function () {
  const d = new Date(this);
  d.setHours(3, 0, 0, 0);
  return d;
}

Date.prototype.toText = function (fileFormat = false) {
  const day = this.getDate()
  const fullDay = day < 10 ? '0' + day : day
  const month = this.getMonth()
  const fillMonth = month < 10 ? '0' + month : month
  const fullYear = this.getFullYear()

  if (!fileFormat) return `${fullDay}.${fillMonth}.${fullYear}`

  const label = monthFullLabels[month]
  return `"${fullDay}" ${label} ${fullYear}г.`
}

Date.prototype.normalize = function (fileFormat = false) {
  const day = this.getDate()
  const fullDay = day < 10 ? '0' + day : day
  const month = this.getMonth()
  const fillMonth = month < 10 ? '0' + month : month
  const fullYear = this.getFullYear()

  if (!fileFormat) return `${fullDay}.${fillMonth}.${fullYear}`

  const label = monthFullLabels[month]
  return `"${fullDay}" ${label} ${fullYear}г.`
}

function getDaysBefore(from, to) {
  const startDate = from instanceof Date ? from : new Date(from).withoutTime()
  const endDate = to instanceof Date ? to : new Date(to).withoutTime()

  return Math.trunc((endDate - startDate) / 1000 / 60 / 60 / 24)
}
module.exports.getDaysBefore = getDaysBefore
module.exports.calculateVacationDuration = function (start, end, holidays) {
  const startDate = start instanceof Date ? start : new Date(start).withoutTime()
  const endDate = end instanceof Date ? end : new Date(end).withoutTime()

  let days = getDaysBefore(startDate, endDate) + 1

  if (holidays && holidays.length) {
    holidays.forEach(day => {
      if (day.withoutTime() >= startDate && day.withoutTime() <= endDate) days--
    })
  }

  return days
}