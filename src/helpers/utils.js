const petrovich = require("petrovich");

module.exports.getDisplayNameAndGender = function (fullName) {
  const [last, first, middle] = fullName.split(' ')

  const displayName = `${last} ${first[0]}. ${middle[0]}.`
  const gender = petrovich.detect_gender(middle)

  return {displayName, gender}
}