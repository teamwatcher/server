const {EmailDataDto} = require("../dto/email-data-dto");
const MailService = require("../../../service/mail-service");
const TeamService = require("../../../service/team-service");

class PlannerRssService {
  async sendVacationComingEmail(orgId, template, buffer, data) {
    const email = new EmailDataDto(template, data)

    email.attachments = [{
        filename: `Заявление на ОТПУСК очередной оплачиваемый ${data.fullName}.docx`,
        content: buffer
      }]

    const teamOwnerEmails = await TeamService.getParentTeamsEmails(orgId, data._user.job?.team)

    const cc = template.addManagers ? teamOwnerEmails : [teamOwnerEmails[0]]
    email.cc.push(...cc)

    email.cc = email.cc.map(x => x.toLowerCase()).filter(x => x !== email.to.toLowerCase())
    email.cc = Array.from(new Set(email.cc))
    return MailService.sendInfoMail(email)
  }
}


module.exports.PlannerRssService = new PlannerRssService()