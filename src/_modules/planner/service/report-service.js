const {scheduleService} = require("./shedule-service");
const {VacationModel} = require("../models/vacation-model");
const {STATUSES} = require("../constants/vacation-constants");
const {FileService} = require("../../../service/file-service");
const {VacationExportDto} = require("../dto/vacation-export-dto");
const {UserService} = require('../../../service/structure/user-service')
const {PlannerManageModel} = require("../models/manage-model");
const {UserVacationExportDto} = require("../dto/user-vacation-export-dto");
const ApiError = require("../../../exceptions/api-error");

class ReportService {
  async getRowsSchedule(orgId, scheduleId) {
    await scheduleService.getModelById(orgId, scheduleId)
    const vacations = await VacationModel
      .find({orgId, scheduleId, status: STATUSES.APPROVED})
      .populate('userId')
      .populate('approvedBy')
      .exec();

    const rows = vacations
      .map(v => new VacationExportDto(v))

    rows.sort((a, b) => {
      return a["ФИО"] > b["ФИО"] ? 0 : -1
    })

    return rows.length ? rows : [new VacationExportDto()]
  }

  async getReportBySchedule(orgId, userId, scheduleId) {
    const rows = await this.getRowsSchedule(orgId, scheduleId)

    const sheet = FileService.getSheet(rows)
    return FileService.getBook([sheet])
  }

  async getReportByOrg(orgId) {
    try {
      const schedules = await scheduleService.getAll(orgId, true)
      const rowsArr = await Promise.all(schedules.map(s => this.getRowsSchedule(orgId, s.id)))

      const sheets = rowsArr.map(rows => FileService.getSheet(rows))

      const names = schedules.map(s => {
        const title = s.title
        const year = s.year
        if (title.length > 26) return title

        return `${title} ${year}г`
      })

      return FileService.getBook(sheets, names)
    }catch (e) {
      throw ApiError.BadRequest(e)
    }
  }

  async getReportByUser(orgId, userId, details) {
    await UserService.getModelById(orgId, userId)
    const {history} = await PlannerManageModel
      .findOne({orgId, userId})
      .populate('history.modifiedBy')
      .populate('history.parentRef')
      .exec()

    const resHistory = details ? history : mergeHistoryEventsWithSameParent(history)

    let rest = 0

    const rows = resHistory.map( h => new UserVacationExportDto(h, rest += h.change))

    const sheet = FileService.getSheet(rows.length ? rows : [new UserVacationExportDto()])

    return FileService.getBook([sheet])
  }
}

function mergeHistoryEventsWithSameParent(arr) {
  // Создаем объект для хранения результата
  const mergedObjects = {};

  // Проходим по каждому элементу в исходном массиве
  arr.forEach(obj => {
    const parentRef = obj.parentRef?._id;

    if (parentRef) {
      if (!mergedObjects[parentRef]) {
        mergedObjects[parentRef] = { parentRef, schedule: obj.parentRef, vacations: 0, change: 0, group: true };
      }

      mergedObjects[parentRef].vacations++;
      mergedObjects[parentRef].change += obj.change;
    } else {
      mergedObjects[obj._id] = obj;
    }
  });

  // Преобразуем объект обратно в массив
  console.log(Object.keys(mergedObjects))

  return Object.values(mergedObjects)
}


module.exports.reportService = new ReportService()