const {ScheduleModel} = require("../models/schedule-model");
const {ScheduleDto} = require("../dto/schedule-dto");
const ApiError = require("../../../exceptions/api-error");
const {SCHEDULE_STATUSES} = require("../constants/schedule-constants");

class ScheduleService {
  async create(orgId, createdBy, data) {
    const schedule = await ScheduleModel.create({
      orgId,
      ...data,
      meta: {
        createdBy
      }
    })

    return new ScheduleDto(schedule)
  }

  async getAll(orgId, isService) {
    const filter = {orgId}
    if (!isService) filter.status = {$ne: SCHEDULE_STATUSES.DRAFT}

    const scheduleList = await ScheduleModel.find(filter)

    return scheduleList.map(schedule => new ScheduleDto(schedule))
  }

  async getModelById(orgId, _id) {
    const schedule = await ScheduleModel.findOne({orgId, _id})
    if (!schedule) {
      throw ApiError.BadRequest(`График не существует`)
    }
    return schedule
  }


  async update(orgId, modifiedBy, scheduleId, data) {
    const schedule = await this.getModelById(orgId, scheduleId)

    Object.assign(schedule, data)
    schedule.meta.modifiedBy = modifiedBy
    schedule.meta.modifiedAt = Date.now()
    return schedule.save()
  }

  async delete(orgId, _id) {
    const doc = await ScheduleModel.findOne({orgId, _id})
    return doc.deleteOne()
  }



  async findById(id) {
    // const org = await OrgModel.findById(id)
    // if (org) return new OrgDto(org)
    //
    // return null
  }


}

module.exports.scheduleService = new ScheduleService()