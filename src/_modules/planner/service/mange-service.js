const {PlannerManageModel} = require("../models/manage-model");
const {ManageEvent} = require("../constants/manage-constants");



class MangeService {
  async init(orgId, userId) {
    return PlannerManageModel.create({orgId, userId,})
  }

  async getByUser(orgId, userId) {
    return PlannerManageModel.findOne({orgId, userId})
  }

  async updateAdditionalDays(orgId, userId, days) {
    const model = await this.getByUser(orgId, userId)

    model.additional = days
    return model.save()
  }

  async updateRestDays(orgId, setBy, userId, days, comment, eventType, absolute = true) {
    const model = await this.getByUser(orgId, userId)

    const change = absolute ? days - model.rest : days
    const rest = absolute ? days : +model.rest + +days

    const event =
      new ManageEvent(eventType, comment, change, setBy, Date.now())

    model.rest = rest
    model.history.push(event)
    return model.save()
  }

  async updateManyRestDays(orgId, setBy, users, days, comment, eventType) {
    return Promise.all(
      users.map(userId => this.updateRestDays(orgId, setBy, userId, days, comment, eventType, false))
    )
  }
}


module.exports.manageService = new MangeService()