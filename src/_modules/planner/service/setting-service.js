const {FileModel} = require("../../../models/file-model");
const {BlankFileName, DefaultPlannerEmailComing} = require("../constants/planner-constants");
const {WordTemplaterService} = require("../../../service/word-templater-service");
const {EmailModel} = require("../../../models/email-model");

class SettingService {
  intiEmail(orgId) {
    return EmailModel.create({orgId, ...(DefaultPlannerEmailComing)})
  }

  async uploadBlank(orgId, userId, data) {
    const name = data.fieldname
    const existFile = await FileModel.findOne({ orgId, name })
    const file = existFile || new FileModel({orgId, name})

    file.original = Buffer.from(data.originalname, 'latin1').toString('utf8')
    file.path = data.path
    file.meta.createdAt = Date.now()
    file.meta.createdBy = userId

    return file.save()
  }

  async createBlankWithData(orgId, data) {
    const {path} = await FileModel.findOne({orgId, name: BlankFileName})
    return WordTemplaterService.getReplacedWordBuffer(path, data)
  }

  async updateEmailComing(orgId, userId, emailId, data) {
    const email = await EmailModel.findOne({orgId, _id: emailId})

    Object.assign(email, data)
    return email.save()
  }
}


module.exports.settingService = new SettingService()