const {VacationModel} = require("../models/vacation-model");
const {calculateVacationDuration} = require("../../../helpers/dateTools");
const {VacationDto} = require("../dto/vacation-dto");
const {EVENTS} = require("../constants/vacation-constants");
const {scheduleService} = require("./shedule-service");
const {UserModel} = require("../../../models/structure/user-model");
const {BlankDataDto} = require("../dto/blank-data-dto");

class VacationService {
  async create(orgId, createdBy, scheduleId, data) {
    const days = await this.getVacationDays(orgId, data.start, data.end, scheduleId)
    const payload = {
      orgId,
      scheduleId,
      ...data,
      days
    }

    payload.history = [EVENTS.CREATED(createdBy, {...payload})]

    const vacation = await VacationModel.create(payload)

    return new VacationDto(vacation)
  }

  async getAll(orgId, depId, isService) {
    const vacationList = await VacationModel
      .find({orgId})
      .populate('userId')
      .exec()

    const vacations = isService
      ? [...vacationList]
      : [...vacationList.filter(v => v.userId.depId.toString() === depId)]

    return vacations
      .map(v => {
        v.userId = v.userId._id
        return v
      })
      .map(vacation => new VacationDto(vacation))
  }

  async getModelById(orgId, _id) {
    const vacation = await VacationModel.findOne({orgId, _id})
    if (!vacation) {
      throw ApiError.BadRequest(`Отпуск не существует`)
    }
    return vacation
  }

  async update(orgId, modifiedBy, vacationId, data, event = undefined) {
    const vacation = await this.getModelById(orgId, vacationId)

    if (data.start && data.end) {
      data.days = await this.getVacationDays(orgId, data.start, data.end, vacation.scheduleId)
    }

    if (!event) event = EVENTS.UPDATED(modifiedBy, data, vacation)

    Object.assign(vacation, data)
    vacation.history.push(event)

    return vacation.save()
  }

  async getVacationDays(orgId, start, end, scheduleId) {
    const {holidayList} = await scheduleService.getModelById(orgId, scheduleId)
    return calculateVacationDuration(start, end, holidayList)
  }

  async delete(orgId, _id) {
    return VacationModel.deleteOne({orgId, _id})
  }

  deleteVacationsByUser(orgId, userId) {
    return VacationModel.deleteMany({orgId, userId})
  }

  async getVacationBlankData(orgId, id) {
    const vacation = await VacationModel.findOne({orgId, _id: id})
    const user = await UserModel
      .findOne({orgId, _id: vacation.userId})
      .populate('job.position')
      .exec()

    return new BlankDataDto(vacation, user)
  }
}

module.exports.vacationService = new VacationService()