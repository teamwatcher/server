const {vacationService} = require("./service/vacation-service");
const {manageService} = require("./service/mange-service");
const {ManageDto} = require("./dto/manage-dto");
const {JobService} = require("../../service/job-service");
const {PlannerRssController} = require("./controllers/planner-rss-controller");
const {settingService} = require("./service/setting-service");

new JobService('0 0 12 * * *', PlannerRssController.RSS)

module.exports.router = require("./router/index")

module.exports.initOrg = function (orgId) {
  return settingService.intiEmail(orgId)
}
module.exports.updatePlannerUserData = async function (orgId, users) {
  for(let user of users) {
    let data = await manageService.getByUser(orgId, user.id)

    if (!data) data = await manageService.init(orgId, user.id)

    user.planner = new ManageDto(data)
  }

  return users
}

module.exports.deletePlannerUserData = function (orgId, userId) {
  return vacationService.deleteVacationsByUser(orgId, userId)
}