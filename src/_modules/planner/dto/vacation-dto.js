module.exports.VacationDto = class VacationDto {
  orgId;
  id;
  userId;
  scheduleId;
  start;
  end;
  days;
  status;
  comment;
  history = [];

  constructor(model) {
    this.id = model._id
    this.orgId = model.orgId
    this.userId = model.userId
    this.scheduleId = model.scheduleId
    this.start = model.start
    this.end = model.end
    this.days = model.days
    this.status = model.status
    this.comment = model.comment
    if (model.history) this.history = [...model.history]
  }
}