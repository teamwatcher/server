module.exports.EmailDataDto = class EmailDataDto {
  data
  subject
  to
  html

  constructor(template, data) {
    this.data = data
    this.subject = this.insertData(template.subject)
    this.to = data.email
    this.html = this.insertData(template.body)
    this.cc = [...template.addCC]
  }

  insertData(str) {
    for(let key in this.data) {
      str = str.replaceAll(`{${key}}`, this.data[key])
    }

    return str
  }
}