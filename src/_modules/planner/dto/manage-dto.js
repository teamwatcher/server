module.exports.ManageDto = class ManageDto {
  orgId;
  id;
  rest;
  additional;
  history = [];

  constructor(model) {
    this.id = model._id
    this.orgId = model.orgId
    this.rest = model.rest
    this.additional = model.additional
    if (model.history) this.history = [...model.history]
  }
  z
}