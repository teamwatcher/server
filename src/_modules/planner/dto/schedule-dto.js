module.exports.ScheduleDto = class ScheduleDto {
  orgId;
  id;
  title;
  year;
  status;
  addVacationDays;
  addIndividualDays
  holidayList = [];


  constructor(model) {
    this.id = model._id
    this.orgId = model.orgId
    this.title = model.title
    this.year = model.year
    this.status = model.status
    this.addVacationDays = model.addVacationDays
    this.addIndividualDays = model.addIndividualDays
    if (model.holidayList) this.holidayList = [...model.holidayList]
  }
}