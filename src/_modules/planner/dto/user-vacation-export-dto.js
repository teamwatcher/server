const {ManageEventTypes} = require("../constants/manage-constants");
module.exports.UserVacationExportDto = class UserVacationExportDto {
  "Тип"
  "Комментарий"
  "Изменение"
  "Остаток"
  "Когда"
  "Кто"

  constructor(model, rest) {
    if (!model.group) {
      switch (model.type) {
        case ManageEventTypes.USER_PERSONAL:
          this["Тип"] = "Персональное изменение";
          break;
        case ManageEventTypes.USER_ORGANIZATION:
          this["Тип"] = "Общее изменение";
          break;
        case ManageEventTypes.AUTO_SCHEDULE:
          this["Тип"] = "АВТО";
          break;
        case ManageEventTypes.AUTO_VACATION:
          this["Тип"] = "АВТО";
          break;
      }

      this["Комментарий"] = model.title
      this["Изменение"] = (model.change > 0 ? "+" : "") + model?.change
      this["Остаток"] = rest
      this["Когда"] = model.modifiedAt
      this["Кто"] = model.modifiedBy?.fullName
    }else {
      this["Комментарий"] = `График "${model.schedule.title}". Отпуска: ${model.vacations} шт`
      this["Изменение"] = (model.change > 0 ? "+" : "") + model?.change
      this["Остаток"] = rest
    }
  }

}