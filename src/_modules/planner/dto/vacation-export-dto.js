module.exports.VacationExportDto = class VacationExportDto {
  "ФИО";
  "C"
  "По"
  "Дней"
  "Утверждено"
  "Когда"

  constructor(model) {
    this["ФИО"] = model?.userId.fullName
    this["C"] = model?.start
    this["По"] = model?.end
    this["Дней"] = model?.days
    this["Утверждено"] = model?.approvedBy.fullName
    this["Когда"] = model?.approvedAt
  }

}