const petrovich = require("petrovich");
const {getDaysBefore} = require("../../../helpers/dateTools");
module.exports.BlankDataDto = class BlankDataDto {
  email
  displayName
  fullName
  fullNameGenitive
  position
  date
  start
  end
  days
  daysShort
  daysBefore

  constructor(vacation, user, date = new Date()) {
    this.email = user.email
    this.displayName = user.displayName
    this.fullName = user.fullName
    this.fullNameGenitive = this.getFullNameGenitive()

    this.position = user?.job?.position?.title || ""
    this.date = date.toText(true)

    this.start = vacation.start.toText(true)
    this.end = vacation.end.toText(true)
    this.daysShort = vacation.days
    this.days = this.getFullDaysLabel()
    this.daysBefore = getDaysBefore(new Date(), vacation.start) + 1

    this._vacation = vacation
    this._user = user
  }

  getFullNameGenitive() {
    const fullName = this.fullName
    const person = {
      first: fullName.split(' ')[1],
      middle: fullName.split(' ')[2],
      last: fullName.split(' ')[0],
    }
    const genitive = petrovich(person, 'genitive')
    return `${genitive.last} ${genitive.first} ${genitive.middle}`
  }

  getFullDaysLabel() {
    const days = this.daysShort
    const mod = days % 10
    switch (true) {
      case (mod === 1): return `${days} календарный день`;
      case (mod >= 2 && mod <= 4): return `${days} календарный дня`;
      default: return `${days} календарный дней`;
    }
  }
}