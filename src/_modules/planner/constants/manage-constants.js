const ManageEventTypes = {
  USER_PERSONAL: 0,
  USER_ORGANIZATION: 1,
  AUTO_SCHEDULE: 2,
  AUTO_VACATION: 3,
}

module.exports.ManageEventTypes = ManageEventTypes
module.exports.ManageEvent = class ManageEvent {
  modifiedBy;
  modifiedAt;
  type;
  title;
  change;
  ref;

  constructor(type, title, change, modifiedBy, modifiedAt, ref, parentRef) {
    this.modifiedBy = modifiedBy
    this.modifiedAt = modifiedAt
    this.type = type
    this.title = title
    this.change = change
    if (ref !== undefined) this.ref = ref
    if (parentRef !== undefined) this.parentRef = parentRef
  }
}