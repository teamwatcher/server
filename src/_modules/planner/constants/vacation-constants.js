const STATUSES = {
  DRAFT: 0,
  ON_APPROVING: 1,
  APPROVED: 2,
  REJECTED: 3
}

const EVENT_STATUSES = {
  CREATED: 0,
  UPDATED: 1,
  SENT_APPROVING: 2,
  CANCEL_APPROVING: 3,
  REJECTED: 4,
  APPROVED: 5,
}

class EventDto {
  start;
  end;
  days;
  comment;

  constructor(model) {
    this.start = model.start
    this.end = model.end
    this.days = model.days
    this.comment = model.comment
  }
}

function createEvent(type, eventBy, newState= undefined, oldState = undefined , comment = undefined) {
  const event = {
    type,
    eventBy,
    eventAt: Date.now()
  }

  if (newState) event.newState = new EventDto( newState )
  if (oldState) event.oldState = new EventDto( oldState )
  if (comment) event.oldState.comment = comment
  return event
}

module.exports.STATUSES = STATUSES
module.exports.EVENT_STATUSES = EVENT_STATUSES

module.exports.EVENTS = {
  CREATED: function (by, newState) {
    return  createEvent(EVENT_STATUSES.CREATED, ...arguments)
  },
  UPDATED: function (by, newState, oldState) {
    return createEvent(EVENT_STATUSES.UPDATED, ...arguments)
  },
  STATUS_EVENT: function (by, status) {
    switch (status) {
      case STATUSES.ON_APPROVING: return createEvent(EVENT_STATUSES.SENT_APPROVING, by);
      case STATUSES.DRAFT: return createEvent(EVENT_STATUSES.CANCEL_APPROVING, by);
      case STATUSES.APPROVED: return createEvent(EVENT_STATUSES.APPROVED, by);
      case STATUSES.REJECTED: return createEvent(EVENT_STATUSES.REJECTED, by);
    }
  },
}