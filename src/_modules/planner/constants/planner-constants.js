module.exports.BlankFileName = 'PlannerBlank'
module.exports.BlankKeywords = [
  { key: 'fullNameGenitive', description: 'ФИО', example: 'Иванова Ивана Ивановича'},
  { key: 'position', description: 'Должность', example: 'Менеджер по продажам'},
  { key: 'date', description: 'Дата подписания', example: (new Date()).toText(true)},
  { key: 'start', description: 'Дата начала отпуска', example: getDateFromNow(4).toText(true) },
  { key: 'end', description: 'Дата конца отпуска', example: getDateFromNow(10).toText(true) },
  { key: 'days', description: 'Дней отпуска', example: '7 календарных дней' },
  { key: 'daysShort', description: 'Дней отпуска', example: '6' },
]

function getDateFromNow(days) {
  const now = new Date()
  now.setDate(now.getDate() + days)
  return now
}
const EmailName = 'PlannerEmailComing'
module.exports.DefaultPlannerEmailComingName = EmailName
module.exports.DefaultPlannerEmailComing = {
  name: EmailName,
  subject: `Уведмление о приближающемся отпуске {fullName}`,
  addManagers: true,
  addCC: [],
  when: [3,7,14],
  body: `
  {displayName}, напоминаем Вам о приближающемся отпуске.<br><br>
  Дней до отпуска: <b>{daysBefore}</b>
  <br><br>
  Отпуск: С <b>{start}</b> По <b>{end}</b><br>
  Дней: <b>{daysShort}</b> 
  </div>
  `

}