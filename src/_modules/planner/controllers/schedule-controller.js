const {scheduleService} = require("../service/shedule-service");

class ScheduleController {
  async create(req, res) {
    const {title, year, addVacationDays, holidayList} = req.body
    const {orgId, user} = req

    const data = {title, year, addVacationDays}
    if (holidayList) data.holidayList = holidayList

    const schedule = await scheduleService.create(orgId, user.id, data)
    res.json(schedule)
  }

  async getAll(req, res) {
    const {orgId, isService} = req

    const scheduleList = await scheduleService.getAll(orgId, isService)

    res.json(scheduleList)
  }

  async update(req, res) {
    const id = req.params.id
    const {data} = req.body
    const {orgId, user} = req

    const schedule = await scheduleService.update(orgId, user.id, id, data)
    res.json(schedule)
  }

  async delete(req, res) {
    const id = req.params.id
    const {orgId} = req

    const schedule = await scheduleService.delete(orgId, id)
    res.json(schedule)
  }

  setStatus(status) {
    return async (req, res) => {
      const id = req.params.id
      const {orgId, user} = req

      const schedule = await scheduleService.update(orgId, user.id, id, {status})
      res.json(schedule)
    }
  }
}

module.exports.scheduleController = new ScheduleController()