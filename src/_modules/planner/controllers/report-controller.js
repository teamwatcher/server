const {reportService} = require("../service/report-service");
const {fileController} = require("../../../controllers/file-controller");

class ReportController {
  async reportBySchedule(req, res) {
    const {id} = req.body
    const {orgId, user} = req

    const buffer = await reportService.getReportBySchedule(orgId, user.id, id)

    await fileController.downloadBuffer(res, "Schedule", 'xlsx', buffer)
  }

  async reportByUser(req, res) {
    const {id, details} = req.body
    const {orgId} = req

    const buffer = await reportService.getReportByUser(orgId, id, details)

    await fileController.downloadBuffer(res, "User", 'xlsx', buffer)
  }

  async reportByOrg(req, res) {
    const {orgId} = req

    const buffer = await reportService.getReportByOrg(orgId)

    await fileController.downloadBuffer(res, "Schedules", 'xlsx', buffer)
  }
}

module.exports.reportController = new ReportController()