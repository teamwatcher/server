const {manageService} = require("../service/mange-service");
const {ManageEventTypes} = require("../constants/manage-constants");

class ManageController {
  async updateAdditionalDays(req, res) {
    const {id, days} = req.body
    const {orgId} = req

    await manageService.updateAdditionalDays(orgId, id, days)

    res.json()
  }

  async updateRestDays(req, res) {
    const {id, days, comment} = req.body
    const {orgId, user} = req

    await manageService.updateRestDays(orgId, user.id, id, days, comment, ManageEventTypes.USER_PERSONAL)

    res.json()
  }

  async updateManyRestDays(req, res) {
    const {users, days, comment} = req.body
    const {orgId, user} = req

    await manageService.updateManyRestDays(orgId, user.id, users, days, comment, ManageEventTypes.USER_PERSONAL)

    res.json()
  }
}

module.exports.manageController = new ManageController()