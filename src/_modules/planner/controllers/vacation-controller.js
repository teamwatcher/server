const {vacationService} = require("../service/vacation-service");
const {EVENTS} = require("../constants/vacation-constants");
const {fileController} = require("../../../controllers/file-controller");
const {settingService} = require("../service/setting-service");

class VacationController {
  async create(req, res) {
    const {scheduleId, start, end, userId, comment} = req.body
    const {orgId, user} = req

    const data = {start, end, userId}
    if (comment) data.comment = comment

    const schedule = await vacationService.create(orgId, user.id, scheduleId, data)
    res.json(schedule)
  }

  async getAll(req, res) {
    const {orgId, depId, isService} = req

    const vacationList = await vacationService.getAll(orgId, depId, isService)
    res.json(vacationList)
  }

  async update(req, res) {
    const id = req.params.id
    const {data} = req.body
    const {orgId, user} = req

    const vacation = await vacationService.update(orgId, user.id, id, data)
    res.json(vacation)
  }

  async delete(req, res) {
    const id = req.params.id
    const {orgId} = req

    const vacation = await vacationService.delete(orgId, id)
    res.json(vacation)
  }

  setStatus(status) {
    return async (req, res) => {
      const id = req.params.id
      const {orgId, user} = req

      const data = {status}
      const event = EVENTS.STATUS_EVENT(user.id, status)

      const vacation = await vacationService.update(orgId, user.id, id, data, event)
      res.json(vacation)
    }
  }

  async getVacationDays(req, res) {
    const {start, end, scheduleId} = req.body
    const {orgId} = req

    const days = await vacationService.getVacationDays(orgId, start, end, scheduleId)
    res.json(days)
  }

  async downloadBlank(req, res) {
    const id = req.params.id
    const {orgId, user} = req

    const data = await vacationService.getVacationBlankData(orgId, id)
    const buffer = await settingService.createBlankWithData(orgId, data)

    fileController.downloadBuffer(res, 'vacation', 'docx', buffer)
  }
}

module.exports.vacationController = new VacationController()
