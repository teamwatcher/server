const {vacationService} = require("../service/vacation-service");
const {settingService} = require("../service/setting-service");
const {PlannerRssService} = require("../service/planner-rss-service");
const {EmailModel} = require("../../../models/email-model");
const {DefaultPlannerEmailComingName} = require("../constants/planner-constants");
const {VacationModel} = require("../models/vacation-model");
const {STATUSES} = require("../constants/vacation-constants");

class PlannerRssController {
  static async orgRSS(orgId) {
    const template = await EmailModel.findOne({orgId, name: DefaultPlannerEmailComingName})
    const dates = template.when.map(days => {
      const date = new Date()
      date.setDate(date.getDate() + days)
      return date.withoutTime()
    })

    const vacations = await VacationModel.find({orgId, status: STATUSES.APPROVED, start: {$in: dates}})

    console.log(`ORG: ${orgId} sending: ${vacations.length}`)
    vacations.map( v => PlannerRssController.sendVacationComingEmail(orgId, v.id, template))
  }

  async RSS() {
    const start = Date.now()
    console.log('START RSS', (new Date()).toText())
    const orgList = await OrgModel.find()

    await Promise.all(orgList.map(org => PlannerRssController.orgRSS(org._id)))
    const end = Date.now()
    console.log(`RSS COMPLETE FOR ${end - start}ms`)
  }

  static async sendVacationComingEmail(orgId, vacationId, template) {
    const data = await vacationService.getVacationBlankData(orgId, vacationId)
    const buffer = await settingService.createBlankWithData(orgId, data)
    await PlannerRssService.sendVacationComingEmail(orgId, template,  buffer, data)
  }
}

module.exports.PlannerRssController = new PlannerRssController()