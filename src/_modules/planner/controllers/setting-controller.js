const {settingService} = require("../service/setting-service");
const {FileModel} = require("../../../models/file-model");
const {BlankFileName, BlankKeywords} = require("../constants/planner-constants");
const {fileController} = require("../../../controllers/file-controller");

class SettingController {
  async uploadBlank(req, res) {
    const {orgId, user, file} = req

    const info = await settingService.uploadBlank(orgId, user.id, file)

    res.json(info)
  }

  async downloadBlank(req, res) {
    const {orgId, user} = req

    const {path} = await FileModel.findOne({orgId, name: BlankFileName})

    fileController.downloadStatic(res, path)
  }

  async downloadBlankExample(req, res) {
    const {orgId, user} = req

    const data = {}
    BlankKeywords.forEach(({key, example}) => {
      data[key] = example
    })
    const buffer = await settingService.createBlankWithData(orgId, data)

    fileController.downloadBuffer(res, 'example', 'docx', buffer)
  }

  async updateEmail(req, res) {
    const {id, data} = req.body
    const {orgId, user} = req

    const email = await settingService.updateEmailComing(orgId, user.id, id, data)

    res.json(email)
  }
}

module.exports.SettingController = new SettingController()