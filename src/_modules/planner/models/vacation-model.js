const {Schema, model} = require('mongoose')
const {STATUSES} = require("../constants/vacation-constants");
const {calculateVacationDuration} = require("../../../helpers/dateTools");
const {PlannerManageModel} = require("./manage-model");

const VacationSchema = new Schema({
  orgId: {type: Schema.Types.ObjectId, ref: 'Struct/Org'},
  userId: {type: Schema.Types.ObjectId, ref: 'Struct/User'},
  scheduleId: {type: Schema.Types.ObjectId, ref: '_Planner/Schedule'},

  start: {type: Date, required: true},
  end: {type: Date, required: true},
  days: {type: Number, required: true},

  status: {type: Number, default: STATUSES.DRAFT},
  comment: {type: String},

  approvedBy: {type: Schema.Types.ObjectId, ref: 'Struct/User'},
  approvedAt: {type: Date},
  history: [
    {
      type: {type: Number, required: true},
      eventBy: {type: Schema.Types.ObjectId, ref: 'Struct/User', required: true},
      eventAt: {type: Date, required: true},
      newState: {
        start: {type: Date},
        end: {type: Date},
        days: {type: Number},
        comment: {type: String}
      },
      oldState: {
        start: {type: Date},
        end: {type: Date},
        days: {type: Number},
        comment: {type: String}
      }
    }
  ]
})

VacationSchema.path('status').set(function (val) {
  this._approved = val === STATUSES.APPROVED;
  this._wasApproved = this.status === STATUSES.APPROVED && !this._approved
  return val
});

VacationSchema.post('deleteOne', {document: true, query: false},async function (data) {
  const doc = await PlannerManageModel.findOne({orgId: data.orgId, userId: data.userId})
  await doc.rollbackVacation(data)
})

VacationSchema.pre('save', {document: true, query: false},async function (next) {
  const curr = this.history.at(-1)
  if (this._approved) {
    this.approvedBy = curr.eventBy
    this.approvedAt = curr.eventAt
  } else {
    this.approvedBy = undefined
    this.approvedAt = undefined
  }

  if (this._approved || this._wasApproved) {
    const doc = await PlannerManageModel.findOne({orgId: this.orgId, userId: this.userId})

    this._approved
      ? await doc.updateByVacation(this)
      : await doc.rollbackVacation(this)
  }

  this.start = this.start.withoutTime()
  this.end = this.end.withoutTime()

  next()
})

VacationSchema.methods.updateDays = function(holidayList){
  this.days = calculateVacationDuration(this.start, this.end, holidayList)
  return this.save()
};

module.exports.VacationModel = model('_Planner/Vacation', VacationSchema)