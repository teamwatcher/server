const {Schema, model} = require('mongoose')
const {ManageEvent, ManageEventTypes} = require("../constants/manage-constants");

const ManageSchema = new Schema({
  orgId: {type: Schema.Types.ObjectId, ref: 'Struct/Org', required: true},
  userId:{type: Schema.Types.ObjectId, ref: 'Struct/User', required: true},
  rest: {type: Number, default: 0},
  additional: {type: Number, default: 0},
  history: [{
    type: {type: Number, required: true},
    title: {type: String, required: true},
    change: {type: Number},
    ref: {type: Schema.Types.ObjectId},
    parentRef: {type: Schema.Types.ObjectId, ref: '_Planner/Schedule'},
    modifiedBy: {type: Schema.Types.ObjectId, ref: 'Struct/User'},
    modifiedAt: {type: Date, default: Date.now()},
  }]
})


ManageSchema.methods.updateByVacation = function (vacation) {
  const title = `Отпуск: C ${vacation.start.toText()} по ${vacation.end.toText()}`
  this.rest -= vacation.days

  const event = new ManageEvent(
    ManageEventTypes.AUTO_VACATION,
    title,
    -1 * vacation.days,
    vacation.approvedBy,
    Date.now(),
    vacation._id,
    vacation.scheduleId
  )

  this.history.push(event)
  return this.save()
}

ManageSchema.methods.rollbackVacation = function (vacation) {
  this.rest += vacation.days

  this.history = this.history.filter(event => {
    return !event.ref || event.ref.toString() !== vacation._id.toString()
  })

  return this.save()
}

ManageSchema.methods.updateBySchedule = function (schedule) {
  const title = `График: "${schedule.title}" от ${schedule.year} г.`
  this.rest += schedule.addVacationDays

  const event = new ManageEvent(
    ManageEventTypes.AUTO_SCHEDULE,
    title,
    schedule.addVacationDays,
    schedule.meta?.modifiedBy,
    schedule.meta?.modifiedAt,
    schedule._id
  )

  this.history.push(event)
  return this.save()
}

ManageSchema.methods.rollbackSchedule = function (schedule) {
  this.rest -= schedule.addVacationDays
  this.history = this.history.filter(event => {
    return !event.ref || event.ref.toString() !== schedule._id.toString()
  })

  return this.save()
}

module.exports.PlannerManageModel = model('_Planner/UserData', ManageSchema)