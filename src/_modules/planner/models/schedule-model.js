const {Schema, model} = require('mongoose')
const {SCHEDULE_STATUSES} = require("../constants/schedule-constants");
const {VacationModel} = require("./vacation-model");
const {PlannerManageModel} = require("./manage-model");
const {meta} = require("../../../models/helper");

const ScheduleSchema = new Schema({
  orgId: {type: Schema.Types.ObjectId, ref: 'Struct/Org', required: true},
  title: {type: String, required: true},
  year: {type: Number, required: true},
  status: {type: Number, default: SCHEDULE_STATUSES.DRAFT},
  addVacationDays: {type: Number, default: 0},
  addIndividualDays: {type: Boolean, default: true},
  holidayList: [{type: Date}],
  meta: meta()
})

ScheduleSchema.path('status').set(function (val) {
  this._activated = this.status === SCHEDULE_STATUSES.DRAFT && val === SCHEDULE_STATUSES.ACTIVE;
  this._deactivated = this.status === SCHEDULE_STATUSES.ACTIVE && val === SCHEDULE_STATUSES.DRAFT;

  return val
});

ScheduleSchema.pre('deleteOne', {document: true, query: false}, async function (next) {
  const docs = await VacationModel.find({scheduleId: this._id})
  for(doc of docs) {
    await doc.deleteOne()
  }

  const users = await PlannerManageModel.find({orgId: this.orgId})
  if (users && users.length) {
    await Promise.all(users.map(doc => doc.rollbackSchedule(this)))
  }
  next()
})



ScheduleSchema.post('save', {document: true, query: false}, async function () {
  const vacations = await VacationModel.find({scheduleId: this._id})
  await Promise.all(vacations.map(doc => doc.updateDays(this.holidayList)))

  if (this._activated || this._deactivated) {
    const users = await PlannerManageModel.find({orgId: this.orgId})

    if (users && users.length) {
      await Promise.all(users.map(doc => {
        return this._activated
          ? doc.updateBySchedule(this)
          : doc.rollbackSchedule(this)
      }))
    }
  }
})

module.exports.ScheduleModel = model('_Planner/Schedule', ScheduleSchema)