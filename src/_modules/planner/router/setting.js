const Router = require('express').Router;
const asyncHandler = require('express-async-handler')
const router = new Router()

const ValidationMiddleware = require("../../../middlewares/validation-middleware");
const ApiError = require("../../../exceptions/api-error");
const {SettingController} = require("../controllers/setting-controller");
const {FileSaverMiddleware} = require("../../../middlewares/filesaver-middleware");
const {BlankFileName} = require("../constants/planner-constants");
const {body} = require("express-validator");



router.post(
  '/blank',
  FileSaverMiddleware(BlankFileName, 'planner'),
  asyncHandler( SettingController.uploadBlank )
)

router.get(
  '/blank',
  asyncHandler( SettingController.downloadBlank )
)


router.get(
  '/blank/example',
  asyncHandler( SettingController.downloadBlankExample )
)

router.post(
  '/email/coming',
  body('id').notEmpty(),
  body('data').notEmpty(),
  asyncHandler( SettingController.updateEmail )
)


module.exports = router