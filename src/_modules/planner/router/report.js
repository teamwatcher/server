const Router = require('express').Router;
const asyncHandler = require('express-async-handler')
const router = new Router()

const {body} = require("express-validator");
const ValidationMiddleware = require("../../../middlewares/validation-middleware");
const ApiError = require("../../../exceptions/api-error");
const {reportController} = require("../controllers/report-controller");



router.post(
  '/schedule',
  body('id').notEmpty(),
  ValidationMiddleware(ApiError.BadRequest()),
  asyncHandler( reportController.reportBySchedule )
);

router.post(
  '/user',
  body('id').notEmpty(),
  ValidationMiddleware(ApiError.BadRequest()),
  asyncHandler( reportController.reportByUser )
);

router.get(
  '/org',
  asyncHandler( reportController.reportByOrg )
);


module.exports = router