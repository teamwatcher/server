const Router = require('express').Router;
const asyncHandler = require('express-async-handler')
const router = new Router()

const {body, param} = require("express-validator");
const ValidationMiddleware = require("../../../middlewares/validation-middleware");
const ApiError = require("../../../exceptions/api-error");
const {SCHEDULE_STATUSES} = require("../constants/schedule-constants");
const {scheduleController} = require("../controllers/schedule-controller");

//create
router.post(
  '/',
  body('title').trim().escape().notEmpty(),
  body('year').isNumeric().notEmpty(),
  ValidationMiddleware(ApiError.BadRequest()),
  asyncHandler( scheduleController.create )
);

//get
router.get(
  '/',
  asyncHandler( scheduleController.getAll )
);

//update
router.post(
  '/:id',
  param('id').notEmpty(),
  body('data').exists(),
  ValidationMiddleware(ApiError.BadRequest()),
  asyncHandler( scheduleController.update )
);

//delete
router.delete(
  '/:id',
  param('id').notEmpty(),
  ValidationMiddleware(ApiError.BadRequest()),
  asyncHandler( scheduleController.delete )
);


//draft
router.get(
  '/:id/draft',
  param('id').notEmpty(),
  ValidationMiddleware(ApiError.BadRequest()),
  asyncHandler( scheduleController.setStatus(SCHEDULE_STATUSES.DRAFT) )
);

//activate
router.get(
  '/:id/activate',
  param('id').notEmpty(),
  ValidationMiddleware(ApiError.BadRequest()),
  asyncHandler( scheduleController.setStatus(SCHEDULE_STATUSES.ACTIVE) )
);


//CLOSE
router.get(
  '/:id/close',
  param('id').notEmpty(),
  ValidationMiddleware(ApiError.BadRequest()),
  asyncHandler( scheduleController.setStatus(SCHEDULE_STATUSES.CLOSED) )
);

module.exports = router