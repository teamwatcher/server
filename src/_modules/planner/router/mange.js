const Router = require('express').Router;
const asyncHandler = require('express-async-handler')
const router = new Router()

const {body} = require("express-validator");
const ValidationMiddleware = require("../../../middlewares/validation-middleware");
const ApiError = require("../../../exceptions/api-error");
const {manageController} = require("../controllers/manage-controller");



router.post(
  '/set/additional',
  body('id').notEmpty(),
  body('days').isNumeric().notEmpty(),
  ValidationMiddleware(ApiError.BadRequest()),
  asyncHandler( manageController.updateAdditionalDays )
);

router.post(
  '/set/rest',
  body('id').notEmpty(),
  body('days').isNumeric().notEmpty(),
  body('comment').isString().trim().notEmpty(),
  ValidationMiddleware(ApiError.BadRequest()),
  asyncHandler( manageController.updateRestDays )
);

router.post(
  '/add/rest/users',
  body('users').notEmpty().isArray(),
  body('days').isNumeric().notEmpty(),
  body('comment').isString().trim().notEmpty(),
  ValidationMiddleware(ApiError.BadRequest()),
  asyncHandler( manageController.updateManyRestDays )
);

module.exports = router