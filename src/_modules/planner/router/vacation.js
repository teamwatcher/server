const Router = require('express').Router;
const asyncHandler = require('express-async-handler')
const router = new Router()

const {body, param} = require("express-validator");
const ValidationMiddleware = require("../../../middlewares/validation-middleware");
const ApiError = require("../../../exceptions/api-error");
const {vacationController} = require("../controllers/vacation-controller");
const {STATUSES} = require("../constants/vacation-constants");
const {PlannerRssController} = require("../controllers/planner-rss-controller");

//create
router.post(
  '/',
  body('start').notEmpty(),
  body('end').notEmpty(),
  body('userId').notEmpty(),
  body('scheduleId').notEmpty(),
  ValidationMiddleware(ApiError.BadRequest()),
  asyncHandler( vacationController.create )
);

//get
router.get(
  '/',
  asyncHandler( vacationController.getAll )
);

//update
router.post(
  '/:id',
  param('id').notEmpty(),
  body('data').exists(),
  ValidationMiddleware(ApiError.BadRequest()),
  asyncHandler( vacationController.update )
);

//delete
router.delete(
  '/:id',
  param('id').notEmpty(),
  ValidationMiddleware(ApiError.BadRequest()),
  asyncHandler( vacationController.delete )
);

//ON_APPROVING
router.get(
  '/:id/approving',
  param('id').notEmpty(),
  ValidationMiddleware(ApiError.BadRequest()),
  asyncHandler( vacationController.setStatus(STATUSES.ON_APPROVING) )
)

//TO DRAFT
router.get(
  '/:id/draft',
  param('id').notEmpty(),
  ValidationMiddleware(ApiError.BadRequest()),
  asyncHandler( vacationController.setStatus(STATUSES.DRAFT) )
)


//APPROVED
router.get(
  '/:id/approve',
  param('id').notEmpty(),
  ValidationMiddleware(ApiError.BadRequest()),
  asyncHandler( vacationController.setStatus(STATUSES.APPROVED) )
)

//REJECTED
router.get(
  '/:id/reject',
  param('id').notEmpty(),
  ValidationMiddleware(ApiError.BadRequest()),
  asyncHandler( vacationController.setStatus(STATUSES.REJECTED) )
)

router.post('/calc/days',
  body('start').exists(),
  body('end').exists(),
  body('scheduleId').notEmpty(),
  ValidationMiddleware(ApiError.BadRequest()),
  asyncHandler( vacationController.getVacationDays )
  )

//DOWNLOAD EXAMPLE
router.get(
  '/:id/blank',
  param('id').notEmpty(),
  ValidationMiddleware(ApiError.BadRequest()),
  asyncHandler( vacationController.downloadBlank )
)

module.exports = router