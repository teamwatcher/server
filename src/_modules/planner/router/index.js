const Router = require('express').Router;
const router = new Router()
const vacationRouter = require('./vacation')
const scheduleRouter = require('./schedule')
const manageRouter = require('./mange')
const reportRouter = require('./report')
const settingRoute = require('./setting')


router.use('/vacation', vacationRouter);
router.use('/schedule', scheduleRouter);
router.use('/manage', manageRouter)
router.use('/report', reportRouter)
router.use('/setting', settingRoute)

module.exports = router