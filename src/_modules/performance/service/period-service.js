const {PeriodModel} = require("../models/period-model");
const {PeriodDto} = require("../dto/period-dto");

class PeriodService {
  async create(orgId, createdBy, data) {
    const period = await PeriodModel.create({
      orgId,
      ...data,
      meta: {
        createdBy
      }
    })

    return new PeriodDto(period)
  }

  async getAll(orgId) {
    const periodList = await PeriodModel.find({orgId})

    return periodList.map(period => new PeriodDto(period))
  }

  async getModelById(orgId, _id) {
    const period = await PeriodModel.findOne({orgId, _id})
    if (!period) {
      throw ApiError.BadRequest(`Отпуск не существует`)
    }
    return period
  }


  async update(orgId, modifiedBy, periodId, data) {
    const period = await this.getModelById(orgId, periodId)

    Object.assign(period, data)
    period.meta.modifiedBy = modifiedBy
    period.meta.modifiedAt = Date.now()
    return period.save()
  }

  async delete(orgId, _id) {
    return PeriodModel.deleteOne({orgId, _id})
  }

  async findById(id) {
    // const org = await OrgModel.findById(id)
    // if (org) return new OrgDto(org)
    //
    // return null
  }


}

module.exports.periodService = new PeriodService()