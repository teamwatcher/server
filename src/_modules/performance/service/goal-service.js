const {GoalModel} = require("../models/goal-model");
const {GoalDto} = require("../dto/goal-dto");
const {VacationModel} = require("../../planner/models/vacation-model");

class GoalService {
  async create(orgId, createdBy, data) {
    const goal = await GoalModel.create({
      orgId,
      ...data,
      meta: {
        createdBy
      }
    })

    return new GoalDto(goal)
  }

  async getAll(orgId) {
    const goalList = await GoalModel.find({orgId})

    return goalList.map(goal => new GoalDto(goal))
  }

  async getModelById(orgId, _id) {
    const goal = await GoalModel.findOne({orgId, _id})
    if (!goal) {
      throw ApiError.BadRequest(`Отпуск не существует`)
    }
    return goal
  }


  async update(orgId, modifiedBy, goalId, data) {
    const goal = await this.getModelById(orgId, goalId)

    Object.assign(goal, data)
    goal.meta.modifiedBy = modifiedBy
    goal.meta.modifiedAt = Date.now()
    return goal.save()
  }

  async delete(orgId, _id) {
    return GoalModel.deleteOne({orgId, _id})
  }


  async findById(id) {
    // const org = await OrgModel.findById(id)
    // if (org) return new OrgDto(org)
    //
    // return null
  }


  deleteGoalsByUser(orgId, userId) {
    return GoalModel.deleteMany({orgId, userId})
  }
}

module.exports.goalService = new GoalService()