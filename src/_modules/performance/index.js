const {goalService} = require("./service/goal-service");
module.exports.router = require("./router/index")

module.exports.deletePerformanceUserData = function (orgId, userId) {
  return goalService.deleteGoalsByUser(orgId, userId)
}