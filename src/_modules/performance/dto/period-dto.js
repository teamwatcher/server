module.exports.PeriodDto = class PeriodDto {
  orgId;
  id;
  title;
  start;
  end;
  duration;
  status;
  progress;
  isStarted;
  isFinished;
  isActive;

  constructor(model) {
    this.id = model._id
    this.orgId = model.orgId
    this.title = model.title
    this.start = model.start
    this.duration = model.duration
    this.end = this.getEnd()
    this.status = model.status
    this.progress = this.getProgress()
    this.isStarted = this.start <= Date.now()
    this.isFinished = this.end <= Date.now()
    this.isActive = this.isStarted && !this.isFinished
  }

  getEnd() {
    const end = new Date(this.start)
    end.setMonth(end.getMonth() + this.duration)
    return end
  }

  getProgress() {
    if (this.end <= Date.now()) return 100
    if (this.start >= Date.now()) return 0

    const durationDays = Math.trunc((this.end - this.start) / 1000 / 60 / 60 / 24)
    const daysBeforeEnd = Math.trunc((this.end - Date.now()) / 1000 / 60 / 60 / 24)

    return ((durationDays - daysBeforeEnd) / durationDays) * 100
  }
}