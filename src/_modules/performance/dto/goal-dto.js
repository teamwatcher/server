const {Schema} = require("mongoose");
const {STATUSES} = require("../constants/goal-constants");
module.exports.GoalDto = class GoalDto {
  orgId;
  userId;
  periodId;
  position;
  title;
  weight;
  description;
  progress;
  status;

  constructor(model) {
    this.id = model._id
    this.orgId = model.orgId
    this.userId = model.userId
    this.periodId = model.periodId

    this.position = model.position
    this.title = model.title
    this.weight = model.weight
    this.description = model.description
    this.progress = model.progress
    this.status = model.status
  }
}
