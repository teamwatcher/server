const {Schema, model} = require('mongoose')
const {STATUSES} = require("../constants/period-constants");

const PeriodSchema = new Schema({
  orgId: {type: Schema.Types.ObjectId, ref: 'Struct/Org', required: true},
  title: {type: String, required: true},
  start: {type: Date, required: true},
  duration: {type: Number, required: true},
  status: {type: Number, default: STATUSES.ACTIVE},
  meta: {
    createdAt: {type: Date, default: Date.now},
    createdBy: {type: Schema.Types.ObjectId, ref: 'Struct/User'},
    modifiedAt: {type: Date},
    modifiedBy: {type: Schema.Types.ObjectId, ref: 'Struct/User'}
  }
})

module.exports.PeriodModel = model('Period', PeriodSchema)