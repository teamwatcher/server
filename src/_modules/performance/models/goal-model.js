const {Schema, model} = require('mongoose')
const {STATUSES} = require("../constants/goal-constants");

const GoalSchema = new Schema({
  orgId: {type: Schema.Types.ObjectId, ref: 'Struct/Org', required: true},
  userId: {type: Schema.Types.ObjectId, ref: 'Struct/User', required: true},
  periodId: {type: Schema.Types.ObjectId, ref: 'Period', required: true},

  position: {type: Number, required: true},
  title: {type: String, required: true},
  weight: {type: Number, required: true},
  description: {type: String},
  progress: {type: Number, default: 0},
  status: {type: Number, default: STATUSES.CREATED},
  meta: {
    createdAt: {type: Date, default: Date.now},
    createdBy: {type: Schema.Types.ObjectId, ref: 'Struct/User'},
    modifiedAt: {type: Date},
    modifiedBy: {type: Schema.Types.ObjectId, ref: 'Struct/User'}
  }
})

module.exports.GoalModel = model('Goal', GoalSchema)