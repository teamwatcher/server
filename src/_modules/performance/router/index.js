const Router = require('express').Router;
const router = new Router()
const periodRouter = require('./period')
const goalRouter = require('./goal')

router.use('/period', periodRouter);
router.use('/goal', goalRouter);

module.exports = router