const Router = require('express').Router;
const asyncHandler = require('express-async-handler')
const router = new Router()

const {body, param} = require("express-validator");
const ValidationMiddleware = require("../../../middlewares/validation-middleware");
const ApiError = require("../../../exceptions/api-error");
const {PeriodController} = require("../controllers/period-controller");
const {STATUSES} = require("../constants/period-constants");

//create
router.post(
  '/',
  body('title').trim().escape().notEmpty(),
  body('start').notEmpty(),
  body('duration').isNumeric().notEmpty(),
  ValidationMiddleware(ApiError.BadRequest()),
  asyncHandler( PeriodController.create )
);

//get
router.get(
  '/',
  asyncHandler( PeriodController.getAll )
);

//update
router.post(
  '/:id',
  param('id').notEmpty(),
  body('data').exists(),
  ValidationMiddleware(ApiError.BadRequest()),
  asyncHandler( PeriodController.update )
);

//delete
router.delete(
  '/:id',
  param('id').notEmpty(),
  ValidationMiddleware(ApiError.BadRequest()),
  asyncHandler( PeriodController.delete )
);



//activate
router.get(
  '/:id/activate',
  param('id').notEmpty(),
  ValidationMiddleware(ApiError.BadRequest()),
  asyncHandler( PeriodController.setStatus(STATUSES.ACTIVE) )
);


//CLOSE
router.get(
  '/:id/close',
  param('id').notEmpty(),
  ValidationMiddleware(ApiError.BadRequest()),
  asyncHandler( PeriodController.setStatus(STATUSES.CLOSED) )
);

module.exports = router