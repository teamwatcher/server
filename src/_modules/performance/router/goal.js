const Router = require('express').Router;
const asyncHandler = require('express-async-handler')
const router = new Router()

const {body, param} = require("express-validator");
const ValidationMiddleware = require("../../../middlewares/validation-middleware");
const ApiError = require("../../../exceptions/api-error");
const {GoalController} = require("../controllers/goal-controller");
const {STATUSES} = require("../constants/goal-constants");

//create
router.post(
  '/',
  body('userId').trim().notEmpty(),
  body('periodId').trim().notEmpty(),
  body('position').isNumeric().notEmpty(),
  body('title').trim().escape().notEmpty(),
  body('weight').isNumeric().notEmpty(),
  ValidationMiddleware(ApiError.BadRequest()),
  asyncHandler( GoalController.create )
);

//get
router.get(
  '/',
  asyncHandler( GoalController.getAll )
);

//update
router.post(
  '/:id',
  param('id').notEmpty(),
  body('data').exists(),
  ValidationMiddleware(ApiError.BadRequest()),
  asyncHandler( GoalController.update )
);

//delete
router.delete(
  '/:id',
  param('id').notEmpty(),
  ValidationMiddleware(ApiError.BadRequest()),
  asyncHandler( GoalController.delete )
);

router.post(
  '/:id/progress',
  param('id').notEmpty(),
  body('progress').exists(),
  ValidationMiddleware(ApiError.BadRequest()),
  asyncHandler( GoalController.updateProgress )
);


// //activate
// router.get(
//   '/:id/activate',
//   param('id').notEmpty(),
//   ValidationMiddleware(ApiError.BadRequest()),
//   asyncHandler( GoalController.setStatus(STATUSES.ACTIVE) )
// );
//
//
// //CLOSE
// router.get(
//   '/:id/close',
//   param('id').notEmpty(),
//   ValidationMiddleware(ApiError.BadRequest()),
//   asyncHandler( GoalController.setStatus(STATUSES.CLOSED) )
// );

module.exports = router