const {goalService} = require("../service/goal-service");

class GoalController {
  async create(req, res) {
    const {userId, periodId, position, title, weight, description, progress} = req.body
    const {orgId, user} = req

    const data = {userId, periodId, position, title, weight}
    if (description) data.description = description
    if (progress) data.progress = progress

    const goal = await goalService.create(orgId, user.id, data)
    res.json(goal)
  }

  async getAll(req, res) {
    const {orgId} = req

    const goalList = await goalService.getAll(orgId)
    res.json(goalList)
  }

  async update(req, res) {
    const id = req.params.id
    const {data} = req.body
    const {orgId, user} = req

    const goal = await goalService.update(orgId, user.id, id, data)
    res.json(goal)
  }

  async updateProgress(req, res) {
    const id = req.params.id
    const {progress} = req.body
    const {orgId, user} = req

    const data = {progress}

    const goal = await goalService.update(orgId, user.id, id, data)
    res.json(goal)
  }

  async delete(req, res) {
    const id = req.params.id
    const {orgId} = req

    const goal = await goalService.delete(orgId, id)
    res.json(goal)
  }

  setStatus(status) {
    return async (req, res) => {
      const id = req.params.id
      const {orgId, user} = req

      const goal = await goalService.update(orgId, user.id, id, {status})
      res.json(goal)
    }
  }
}

module.exports.GoalController = new GoalController()