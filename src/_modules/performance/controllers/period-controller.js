const {periodService} = require("../service/period-service");

class PeriodController {
  async create(req, res) {
    const {title, start, duration} = req.body
    const {orgId, user} = req

    const data = {title, start, duration}

    const period = await periodService.create(orgId, user.id, data)
    res.json(period)
  }

  async getAll(req, res) {
    const {orgId} = req

    const periodList = await periodService.getAll(orgId)
    res.json(periodList)
  }

  async update(req, res) {
    const id = req.params.id
    const {data} = req.body
    const {orgId, user} = req

    const period = await periodService.update(orgId, user.id, id, data)
    res.json(period)
  }

  async delete(req, res) {
    const id = req.params.id
    const {orgId} = req

    const period = await periodService.delete(orgId, id)
    res.json(period)
  }

  setStatus(status) {
    return async (req, res) => {
      const id = req.params.id
      const {orgId, user} = req

      const period = await periodService.update(orgId, user.id, id, {status})
      res.json(period)
    }
  }
}

module.exports.PeriodController = new PeriodController()