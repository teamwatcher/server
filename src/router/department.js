const Router = require('express').Router;
const {body, param} = require("express-validator");
const asyncHandler = require('express-async-handler')
const ValidationMiddleware = require('../middlewares/validation-middleware')
const ApiError = require("../exceptions/api-error");
const {DepController} = require("../controllers/dep-controller");

const router = new Router()

router.get(
  '/',
  asyncHandler( DepController.getAll )
)

router.post(
  '/',
  body('title').trim().escape().notEmpty(),
  ValidationMiddleware(ApiError.BadRequest()),
  asyncHandler( DepController.create)
)

router.post(
  '/:id',
  param('id').notEmpty(),
  body('title').trim().escape().notEmpty(),
  ValidationMiddleware(ApiError.BadRequest()),
  asyncHandler( DepController.update)
)

router.delete(
  '/:id',
  param('id').notEmpty(),
  ValidationMiddleware(ApiError.BadRequest("ID не может быть путым")),
  asyncHandler( DepController.delete)
)


module.exports = router