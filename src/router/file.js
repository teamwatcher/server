const Router = require('express').Router;
const {fileController} = require('../controllers/file-controller')
const {body, param} = require("express-validator");
const asyncHandler = require('express-async-handler')
const ValidationMiddleware = require("../middlewares/validation-middleware");
const ApiError = require("../exceptions/api-error");
const multer  = require('multer')
const upload = multer()

const router = new Router()

router.post(
  '/parse/users',
  upload.single('import'),
  ValidationMiddleware(ApiError.BadRequest()),
  asyncHandler( fileController.parseUsersExcel )
)

router.get(
  '/get/template/import',
  asyncHandler( fileController.downloadTemplateImport )
)

module.exports = router