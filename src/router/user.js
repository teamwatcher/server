const Router = require('express').Router;
const userController = require('../controllers/user-controller')
const {body, param} = require("express-validator");
const asyncHandler = require('express-async-handler')
const ValidationMiddleware = require("../middlewares/validation-middleware");
const ApiError = require("../exceptions/api-error");

const router = new Router()

router.get(
  '/',
  asyncHandler( userController.getAll )
)


router.get(
  '/export',
  asyncHandler( userController.export )
)

router.get(
  '/:userId',
  param('userId').notEmpty(),
  ValidationMiddleware(ApiError.BadRequest()),
  asyncHandler( userController.getById )
)

router.post(
  '/:userId',
  param('userId').notEmpty(),
  body('user').notEmpty(),
  ValidationMiddleware(ApiError.BadRequest()),
  asyncHandler( userController.update )
)

router.delete(
  '/:userId',
  param('userId').notEmpty(),
  ValidationMiddleware(ApiError.BadRequest()),
  asyncHandler( userController.delete )
)

router.post(
  '/:userId/task',
  body('id').notEmpty(),
  ValidationMiddleware(ApiError.BadRequest()),
  asyncHandler( userController.taskAdd )
)

//invite
router.get(
  '/invite/:userId',
  param('userId').notEmpty(),
  ValidationMiddleware(ApiError.BadRequest()),
  asyncHandler( userController.invite )
)


router.delete(
  '/:userId/task/:id',
  param('id').notEmpty(),
  ValidationMiddleware(ApiError.BadRequest()),
  asyncHandler( userController.taskRemove )
)

module.exports = router