const Router = require('express').Router;
const {body, param} = require("express-validator");
const asyncHandler = require('express-async-handler')
const ValidationMiddleware = require('../middlewares/validation-middleware')
const ApiError = require("../exceptions/api-error");
const AuthMiddleware = require("../middlewares/auth-middleware");
const {OrgController} = require("../controllers/org-controller");

const router = new Router()


router.post(
  '/create',
  body('title').trim().escape().notEmpty(),
  body('ownerId').notEmpty(),
  ValidationMiddleware(ApiError.BadRequest()),
  asyncHandler( OrgController.create)
)

router.post(
  '/update',
  body('title').trim().escape().notEmpty(),
  ValidationMiddleware(ApiError.BadRequest()),
  AuthMiddleware,
  asyncHandler( OrgController.update)
)

router.post(
  '/bind',
  body('orgId').notEmpty(),
  body('userId').notEmpty(),
  ValidationMiddleware(ApiError.BadRequest("ID не может быть путым")),
  asyncHandler( OrgController.bind)
)

router.get(
  '/get/:id',
  param('id').notEmpty(),
  ValidationMiddleware(ApiError.BadRequest("ID не может быть путым")),
  asyncHandler( OrgController.getById)
)


module.exports = router