const Router = require('express').Router;
const {body, param} = require("express-validator");
const asyncHandler = require('express-async-handler')
const ValidationMiddleware = require('../middlewares/validation-middleware')
const teamController = require('../controllers/team-controller')
const ApiError = require("../exceptions/api-error");

const router = new Router()

//get all
router.get(
  '/',
  asyncHandler( teamController.getAll)
)

//create
router.post(
  '/',
  body('title').trim().escape().notEmpty(),
  body('depId').notEmpty(),
  ValidationMiddleware(ApiError.BadRequest()),
  asyncHandler( teamController.create)
)

//update
router.post(
  '/:id',
  param('id').notEmpty(),
  body('title').trim().escape().notEmpty(),
  ValidationMiddleware(ApiError.BadRequest()),
  asyncHandler( teamController.update)
)

//delete
router.delete(
  '/:id',
  param('id').notEmpty(),
  ValidationMiddleware(ApiError.BadRequest("ID не может быть путым")),
  asyncHandler( teamController.delete)
)

module.exports = router