const Router = require('express').Router;
const authController = require('../controllers/auth-controller')
const asyncHandler = require('express-async-handler')
const ValidationMiddleware = require('../middlewares/validation-middleware')
const ApiError = require("../exceptions/api-error");
const AuthValidators = require('../validators/auth-validators')
const {body} = require("express-validator");

const router = new Router()


router.post(
  '/registration',
  AuthValidators.email,
  AuthValidators.passwordMin,
  AuthValidators.passwordMax,
  body('org.title').trim().escape(),
  ValidationMiddleware(ApiError.BadRequest("Ошибка при валидации")),
  asyncHandler( authController.registration)
)

router.post(
  '/preregistration',
  AuthValidators.email,
  body('user').notEmpty(),
  ValidationMiddleware(ApiError.BadRequest("Ошибка при валидации")),
  asyncHandler( authController.preregistration)
)

router.post(
  '/login',
  AuthValidators.email,
  AuthValidators.passwordMin,
  AuthValidators.passwordMax,
  ValidationMiddleware(ApiError.BadRequest("Ошибка при валидации")),
  asyncHandler( authController.login)
)


router.post(
  '/reset',
  AuthValidators.email,
  ValidationMiddleware(ApiError.BadRequest("Ошибка при валидации")),
  asyncHandler( authController.reset)
)

router.get('/activate/:link', asyncHandler( authController.activate) )
router.get('/invitation/:link', asyncHandler( authController.invitation) )
router.post(
  '/invitation/complete',
  AuthValidators.passwordMin,
  AuthValidators.passwordMax,
  body('code').exists().notEmpty(),
  ValidationMiddleware(ApiError.BadRequest("Ошибка при валидации")),
  asyncHandler( authController.invitationComplete)
)

router.get('/logout', asyncHandler( authController.logout) )
router.get('/refresh', asyncHandler( authController.refresh) )

module.exports = router