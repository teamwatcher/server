const Router = require('express').Router;
const {body, param} = require("express-validator");
const asyncHandler = require('express-async-handler')
const ValidationMiddleware = require('../middlewares/validation-middleware')
const positionController = require('../controllers/position-controller')
const ApiError = require("../exceptions/api-error");

const router = new Router()

//get all
router.get(
  '/',
  asyncHandler( positionController.getAll)
)

//create
router.post(
  '/',
  body('title').trim().escape().notEmpty(),
  body('depId').notEmpty(),
  ValidationMiddleware(ApiError.BadRequest()),
  asyncHandler( positionController.create)
)

//update
router.post(
  '/:id',
  param('id').notEmpty(),
  body('title').trim().escape().notEmpty(),
  ValidationMiddleware(ApiError.BadRequest()),
  asyncHandler( positionController.update)
)

//delete
router.delete(
  '/:id',
  param('id').notEmpty(),
  ValidationMiddleware(ApiError.BadRequest("ID не может быть путым")),
  asyncHandler( positionController.delete)
)

//get by id
// router.get(
//   '/:id',
//   param('id').notEmpty(),
//   ValidationMiddleware(ApiError.BadRequest("ID не может быть путым")),
//   asyncHandler( orgController.getById)
// )


module.exports = router