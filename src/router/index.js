const Router = require('express').Router;
const AuthMiddleware = require("../middlewares/auth-middleware");
const router = new Router()

const authRouter = require('./auth')
const orgRouter = require('./organization')
const depRouter = require('./department')
const taskRouter = require('./task')
const positionRouter = require('./position')
const teamRouter = require('./team')
const userRouter = require('./user')
const fileRouter = require('./file')

const {router: PlannerRouter} = require('../_modules/planner/index')
const {router: PerformanceRouter} = require('../_modules/performance/index')


router.use('/auth', authRouter);
router.use('/org', orgRouter);
router.use('/dep', AuthMiddleware, depRouter);
router.use('/task', AuthMiddleware, taskRouter);
router.use('/position', AuthMiddleware, positionRouter)
router.use('/team', AuthMiddleware,teamRouter)
router.use('/user', AuthMiddleware, userRouter)
router.use('/file', AuthMiddleware, fileRouter)

router.use('/planner', AuthMiddleware, PlannerRouter)
router.use('/performance', AuthMiddleware, PerformanceRouter)


module.exports = router