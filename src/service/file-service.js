const reader = require('xlsx')
const xlsx = require("xlsx");

class FileService {
  async saveFileToStorage(orgId, fileName, file) {

  }


  parseXLSX(file) {
      const data = reader.read(file.buffer, {type:'buffer'})
      const wsname = data.SheetNames[0];
      const ws = data.Sheets[wsname];
      const json = reader.utils.sheet_to_json(ws, {header: 1});
      return json.filter(d => !d.every(x => x == undefined))
  }

  getSheet(rows, minW = 10, autoFilter = true) {
    const worksheet = xlsx.utils.json_to_sheet(rows);

    const columnWidths = rows.map(row => {
      return Object.entries(row).map(([key, value]) => {
        const dataWidth = value
          ? value instanceof Date
            ? 10
            : value.toString().length
          : 0

        const keyWidth = key.toString().length

        return Math.max(minW, dataWidth, keyWidth);
      });
    });

    const maxWidths = columnWidths.reduce((maxWidths, widths) =>
        widths.map((width, i) => Math.max(maxWidths[i] || 0, width))
      , []);

    const padding = autoFilter ? 4 : 2
    worksheet['!cols'] = maxWidths.map(width => ({ width: width + padding })); // Adding a little padding

    if (autoFilter) {
      const endColIndex = maxWidths.length - 1;
      worksheet['!autofilter'] = {
        ref: xlsx.utils.encode_range({s: {r: 0, c: 0}, e: {r: 0, c: endColIndex}})
      };
    }

    return worksheet
  }
  getBook(sheets, names = []) {
    const workbook = xlsx.utils.book_new();


    sheets.forEach((sheet, idx) => {
      const name = validateAndFixSheetName(names[idx], idx + 1)
      xlsx.utils.book_append_sheet(workbook, sheet, name);
    })

    return xlsx.write(workbook, { type: 'buffer', bookType: 'xlsx' });
  }
}

function validateAndFixSheetName(name, idx) {
  if (typeof name !== 'string' || !name) {
    return 'Sheet' + idx;
  }

  const sanitizedString = name.replace(/[:?/*\[\].]/g, '');
  return sanitizedString.slice(0, 31);
}

module.exports.FileService = new FileService()