const schedule = require('node-schedule');

class JobService {
  runner;

  constructor(time, job) {
    this.runner = schedule.scheduleJob(time, job);
  }
}

module.exports.JobService = JobService