const nodemailer = require('nodemailer')
const {confirmEmail, inviteEmail, resetPasswordEmail} = require("./mail-templates");

class MailService {
  constructor() {
    this.noreplayTransprter = nodemailer.createTransport({
      host: process.env.SMTP_HOST,
      port: process.env.SMTP_PORT,
      secure: true,
      auth: {
        user: process.env.SMTP_NOREPLY,
        pass: process.env.SMTP_NOREPLY_PWD
      }
    })

    this.infoTransprter = nodemailer.createTransport({
      host: process.env.SMTP_HOST,
      port: process.env.SMTP_PORT,
      secure: true,
      auth: {
        user: process.env.SMTP_INFO,
        pass: process.env.SMTP_INFO_PWD
      }
    })
  }

  async sendActivationMail(to, link) {
    await this.noreplayTransprter.sendMail({
      from: process.env.SMTP_NOREPLY,
      to,
      subject: "Завершение регистрации в приложении " + process.env.APP_NAME,
      html: confirmEmail(link)
    })
  }

  async sendInviteLink(to, link, who, whom) {
    await this.noreplayTransprter.sendMail({
      from: process.env.SMTP_NOREPLY,
      to,
      subject: "Приглашение в " + process.env.APP_NAME,
      html: inviteEmail(who, whom, link)
    })
  }

  async sendResetPasswordMail(to, password) {
    await this.noreplayTransprter.sendMail({
      from: process.env.SMTP_NOREPLY,
      to,
      subject: "Сброс пароля в " + process.env.APP_NAME,
      html: resetPasswordEmail(password)
    })
  }

  async sendInfoMail(email) {
    await this.infoTransprter.sendMail({
      from: process.env.SMTP_INFO,
      ...email
    })
  }
}

module.exports = new MailService()