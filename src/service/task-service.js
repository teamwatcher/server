const ApiError = require("../exceptions/api-error");
const TaskDto = require("../dtos/task-dto");
const {TaskModel} = require("../models/structure/task-model");

class TaskService {
  async create(orgId, createdBy, depId, data) {
    const task = await TaskModel.create({
      orgId,
      depId,
      meta: { createdBy },
      ...data
    })

    return new TaskDto(task)
  }

  async getAll(orgId, depId, isService) {
    const filter = {orgId}
    if (!isService) filter.depId = depId

    const items = await TaskModel.find(filter)

    return items.map(task => new TaskDto(task))
  }

  async getModelById(orgId, taskId) {
    const task = await TaskModel.findOne({orgId, _id: taskId})
    if (!task) {
      throw ApiError.BadRequest(`Задачи не существует`)
    }

    return task
  }

  async update(orgId, updateBy, taskId, data) {
    const task = await this.getModelById(orgId, taskId)

    Object.assign(task, data)
    task.meta.modifiedAt = new Date()
    task.meta.modifiedBy = updateBy
    return task.save()
  }

  async delete(orgId, id) {
    const data = await this.getModelById(orgId, id)
    return data.deleteOne()
  }
}

module.exports = new TaskService()