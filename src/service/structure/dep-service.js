const ApiError = require("../../exceptions/api-error");
const {DepModel} = require("../../models/structure/dep-model");
const DepDto = require("../../dtos/dep-dto");

class DepService {
  async getAll(orgId, depId, isService) {
    const filter = {orgId}
    if (!isService) filter._id = depId

    const items = await DepModel.find(filter)

    return items.map(item => new DepDto(item))
  }

  async getModelById(orgId, id) {
    const data = await DepModel.findOne({_id: id, orgId })
    if (!data) throw ApiError.BadRequest(`Депортамент не найден`)

    return data
  }

  async getRootModel(orgId) {
    const data = await DepModel.findOne({orgId, root: true})
    if (!data) throw ApiError.BadRequest(`Депортамент не найден`)

    return data
  }

  async create(orgId, by, data) {

  // async create(orgId, by, title, ownerId, parentId = undefined, root = false, displayTitle) {
    if (!data.title) throw ApiError.BadRequest(`Навзание не может быть пустым`)
    if (!data.displayTitle) data.displayTitle = data.title
    if (data.root === undefined) data.root = false

    const org = await DepModel.create({
      orgId,
      ...data,
      meta: {
        createdBy: by,
      }
    })

    return new DepDto(org)
  }

  async update(orgId, id, by, title, ownerId) {
    const dep = await this.getModelById(orgId, id)

    dep.title = title
    dep.ownerId = ownerId
    dep.meta.modifiedBy = by
    dep.meta.modifiedAt = new Date()
    return dep.save()
  }

  async delete(orgId, id) {
    const dep = await this.getModelById(orgId, id)
    return dep.deleteOne()
  }
}

module.exports.DepService = new DepService()