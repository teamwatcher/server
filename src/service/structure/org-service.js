const ApiError = require("../../exceptions/api-error");
const OrgDto = require("../../dtos/org-dto");
const {OrgModel} = require("../../models/structure/org-model");
const {initOrg} = require("../../_modules/planner");
const mongoose = require("mongoose");
const {UserModel} = require("../../models/structure/user-model");

class OrgService {
  async create(title, ownerId, depId) {
    if (!title) throw ApiError.BadRequest(`Навзание организации не может быть пустым`)
    const candidate = await OrgModel.findOne({ownerId})
    if (candidate) {
      throw ApiError.BadRequest(`У пользователя уже есть организация`)
    }

    const orgId = new mongoose.Types.ObjectId()

    const data = await initOrg(orgId)
    const org = await OrgModel.create({
      _id: orgId,
      rootDep: depId,
      ownerId,
      title,
      emails: [data._id],
      meta: {createdBy: ownerId}
    })

    return new OrgDto(org)
  }

  async update(orgId, userId, data) {
    const org = await OrgModel.findById(orgId)
    if (!org) {
      throw ApiError.BadRequest("Организация не найдена")
    }

    if (org.ownerId.toString() !== userId) {
      throw ApiError.BadRequest("Недостаточно прав")
    }

    Object.assign(org, data)
    return org.save()
  }

  async findById(_id) {
    try {
      const org = await OrgModel.findOne({_id})
        .populate('rootDep')
        .populate('emails')
        .populate('files')
        .exec()

      org.rootDep.ownerId = await UserModel.findOne({_id: org.rootDep.ownerId})

      return new OrgDto(org)
    }catch (e) {
      throw ApiError.BadRequest(`Организация не найдена`)
    }
  }
}

module.exports.OrgService = new OrgService()