const {UserModel} = require('../../models/structure/user-model');
const ApiError = require('../../exceptions/api-error')
const UserDto = require("../../dtos/user-dto");
const mailService = require('../mail-service')
const {isEmail, escape} = require("validator");
const {UserExportDto} = require("../../dtos/user-export-dto");
const {FileService} = require("../file-service");

class UserService {
  async getAllUsers(orgId, depId, isService) {
    const filter = {orgId}
    if (!isService) filter.depId = depId

    const items = await UserModel.find(filter)

    return Promise.all(items.map(user => UserService.updateUserData(user)))
  }

  static async updateUserData(data) {
    return new UserDto(data)
  }

  async getModelById(orgId, userId) {
    const user = await UserModel.findOne({orgId, _id: userId})
    if (!user) {
      throw ApiError.BadRequest(`Пользователь не найден`)
    }

    return user
  }

  async getById(orgId, userId) {
    const model = await this.getModelById(orgId, userId)
    return new UserDto(model)
  }

  async update(orgId, updateBy, userId, data) {
    const user = await this.getModelById(orgId, userId)

    Object.assign(user, data)
    user.meta.modifiedAt = new Date()
    user.meta.modifiedBy = updateBy
    return user.save()
  }

  async delete(orgId, userId) {
    const user = await this.getModelById(orgId, userId)
    return user.deleteOne()
  }

  async invite(to, link, who, whom) {
    return mailService.sendInviteLink(to, `${process.env.API_URL}/auth/invitation/${link}`, who, whom)
  }

  async parseXLSXData(header, data) {
    try {
      const headersName = [
        {name: "email", validation: isEmail, unique: true},
        {name: "fullName", validation: val => val.split(' ').length >= 3}
      ]

      header = header.map(x => x.toLowerCase())
      headersName.forEach(field => {
        const colIdx = header.findIndex(h => h === field.name.toLowerCase())
        if (colIdx === -1) throw ApiError.BadRequest("Не найдено обязательное поле: " + field.name)

        field.colIdx = colIdx
      })


      const validUsers = []
      const invalidUsers = []

      for (const row of data) {
        const rowIdx = data.indexOf(row);
        let errors = []
        const user = {}

        for(let {name, colIdx, validation, unique} of headersName) {
          const value = escape(row[colIdx].toString())
          user[name] = value
          if (!validation.call(this, value)) {
            errors.push(`Значение <b>${value}</b> для поля <b>${name}</b> недопустимо`)
          }
          const findOptions = {}
          findOptions[name] = value.toLowerCase()
          const existingUser = await UserModel.findOne(findOptions)
          if (unique && existingUser) {
            errors.push(`Пользователь с таким <b>${name}</b> существет`)
          }
          if (unique && validUsers.some(u => u[name].toLowerCase() === value.toLowerCase())) {
            errors.push(`Поле <b>${name}</b> должно быть уникальным`)
          }
        }

        if (errors.length) {
          invalidUsers.push({row: rowIdx + 2, errors, user})
        } else {
          validUsers.push(user)
        }
      }

      return {validUsers, invalidUsers}
    }catch (e) {
      throw e
    }
  }

  async exportUsers(orgId) {
    const users = await UserModel
      .find({ orgId })
      .populate('job.position')
      .populate('job.team')
      .exec();

    const rows = users.map(user => new UserExportDto(user))

    const sheet = FileService.getSheet(rows)
    return FileService.getBook([sheet])
 }

  ROLE = {
    setUserRole: async (userId, role, data = {}) => {
      const user = await UserModel.findOne({_id: userId})
      if (user && !user.roles.includes(role)) {
        user.roles.push(role)

        Object.assign(user, data)

        await user.save()
      }
    },
    unsetUserRole: async (Model, userId, role) => {
      const items = await Model.find({ownerId: userId})

      if (items.length <= 1) {
        const user = await UserModel.findOne({ _id: userId })

        user.roles = user.roles.filter(r => r !== role)
        await user.save()
      }
    }
  }

  TASK = {
    remove: async (orgId, userId, id) => {
      return UserModel.findOneAndUpdate(
        {orgId, _id: userId},
        { $pull: { "job.taskList": id } },
        { new: true },
      )
    },
    add: async (orgId, userId, id) => {
      const user = await this.getModelById(orgId, userId)
      if (user.job.taskList.includes(id)) return user

      user.job.taskList.push(id)
      return user.save()
    },
  }
}


module.exports.UserService = new UserService()
