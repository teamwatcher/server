const ApiError = require("../exceptions/api-error");
const PositionDto = require("../dtos/position-dto");
const {PositionModel} = require("../models/structure/position-model");

class PositionService {
  async getModelById(orgId, taskId) {
    const position = await PositionModel.findOne({orgId, _id: taskId})
    if (!position) {
      throw ApiError.BadRequest(`Должности не существует`)
    }

    return position
  }

  async create(orgId, by, data) {
    const position = await PositionModel.create({
      orgId,
      ...data,
      meta: {
        createdBy: by
      }
    })

    return new PositionDto(position)
  }

  async update(orgId, by, id, title, displayTitle) {
    const position = await this.getModelById(orgId, id)

    position.title = title
    position.displayTitle = displayTitle
    position.meta.modifiedBy = by
    position.meta.modifiedAt = new Date()
    return position.save()
  }

  async delete(orgId, id) {
    const data = await this.getModelById(orgId, id)
    return data.deleteOne()
  }

  async getAll(orgId, depId, isService) {
    const filter = {orgId}
    if (!isService) filter.depId = depId

    const items = await PositionModel.find(filter)

    return items.map(position => new PositionDto(position))
  }
}

module.exports = new PositionService()