const ApiError = require("../exceptions/api-error");
const TeamDto = require("../dtos/team-dto");
const OrgService = require("./structure/org-service");
const {UserService} = require("./structure/user-service");
const {TeamModel} = require("../models/structure/team-model");

class TeamService {
  async create(orgId, depId, by, payload) {
    const data = {
      orgId,
      depId,
      meta: {
        createdBy: by
      }
    }

    for(let key in payload) {
      if (payload[key]) {
        data[key] = payload[key]
      }
    }

    const team = await TeamModel.create(data)

    return new TeamDto(team)
  }

  async getAll(orgId, depId, isService) {
    const filter = {orgId}
    if (!isService) filter.depId = depId

    const items = await TeamModel.find(filter)

    return items.map(team => new TeamDto(team))
  }

  async getModelById(orgId, teamId) {
    const team = await TeamModel.findOne({orgId, _id: teamId})
    if (!team) {
      throw ApiError.BadRequest(`Команды не существует`)
    }

    return team
  }

  async update(orgId, by, teamId, data) {
    const team = await this.getModelById(orgId, teamId)

    Object.assign(team, data)
    team.meta.modifiedBy = by
    team.meta.modifiedAt = new Date()

    return team.save()
  }

  async delete(orgId, teamId) {
    const team = await TeamModel.findOne({orgId, _id: teamId})
    return team.deleteOne()
  }




  async getTeamsTree(orgId) {
    const organization = await OrgService.getModelById(orgId)
    const teams = await this.getAll(orgId)

    let tree = {...organization, children: [], root: true}

    let tempTeams = {}
    teams.forEach(team => {
      tempTeams[team.id] = team
    })

    for (let id in tempTeams) {
      let team = tempTeams[id]
      let parentId = team.parentId?.toString()

      if (parentId === '' || parentId === undefined) {
        continue
      }

      if (!tempTeams[parentId].children) {
        tempTeams[parentId].children = []
      }
      if (!tempTeams[parentId].children.find(child => child.id === team.id)) {
        tempTeams[parentId].children.push(team)
      }
    }

    tree.children.push(...Object.values(tempTeams).filter(t => t.parentId === '' || t.parentId === undefined))

    return [tree]
  }

  async getInnerTeamsById(orgId, id) {
    const childIds = [];
    const teams = await this.getAll(orgId)

    // Рекурсивная функция для поиска дочерних id
    function findChildIds(teams, id) {
      for (let i = 0; i < teams.length; i++) {
        if (teams[i].parentId === id) {
          childIds.push(teams[i].id);
          findChildIds(teams, teams[i].id); // Рекурсивный вызов для поиска дочерних id
        }
      }
    }

    findChildIds(teams, id);
    return childIds;
  }

  async getParentTeamsById(orgId, targetId) {
    const teams = await this.getAll(orgId)
    const teamsMap = {};

    teams.forEach(team => {
      teamsMap[team.id] = { ...team, children: [] };
    });
    let currentTeam = teamsMap[targetId]
    const parents = []

    if (!currentTeam) return parents

    do {
      parents.push(currentTeam.id)
      currentTeam = teamsMap[currentTeam.parentId]
    }while(currentTeam)

    return parents
  }

  async getParentTeamsEmails(orgId, targetId) {
    const teams = await this.getAll(orgId)
    const org = await OrgService.getModelById(orgId)
    const teamsMap = {};

    teams.forEach(team => {
      teamsMap[team.id] = { ...team, children: [] };
    });
    let currentTeam = teamsMap[targetId]
    const parents = []

    if (currentTeam) {
      do {
        if (currentTeam.ownerId) parents.push(currentTeam.ownerId.toString())
        currentTeam = teamsMap[currentTeam.parentId]
      } while (currentTeam)
    }
    parents.push(org.ownerId.toString())
    const users = await UserService.getAllUsers(orgId)
    return parents.map(id => users.find(user => user.id.toString() === id)?.email)
  }
}

module.exports = new TeamService()