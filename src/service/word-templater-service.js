const PizZip = require("pizzip");
const templater = require("docxtemplater");

const fs = require("fs");
const path = require("path");

class WordTemplaterService {
  async getReplacedWordBuffer(filePath, data) {
    const content = fs.readFileSync(
      path.resolve('./', filePath),
      "binary"
    );

    const zip = new PizZip(content);

    const doc = new templater(zip, {
      paragraphLoop: true,
      linebreaks: true,
    });

    doc.render(data)

    return doc.getZip().generate({
      type: "nodebuffer",
      compression: "DEFLATE",
    });
  }
}

module.exports.WordTemplaterService = new WordTemplaterService()