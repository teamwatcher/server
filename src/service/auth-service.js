const ApiError = require('../exceptions/api-error')
const bcrypt = require('bcrypt')
const uuid = require("uuid");
const mailService = require('./mail-service')
const tokenService = require('./token-service')
const UserDto = require("../dtos/user-dto");
const {STATUSES, ROLES} = require('../constants/user-constants')
const {generate} = require("generate-password");
const {UserModel} = require("../models/structure/user-model");

class AuthService {
  async validateCandidate({email}) {
    const candidate = await UserModel.findOne({email})
    if (candidate) {
      throw ApiError.BadRequest(`Пользователь с почтой ${email} уже существет`)
    }
  }

  async preregistration(orgId, email, data) {
    await this.validateCandidate({email})

    const user = await UserModel.create({
      orgId,
      email,
      ...data,
      meta: {
        registerMethod: "link"
      }
    })

    return new UserDto(user)
  }

  async registration(id, email, password, fullName, orgId, depId) {
    await this.validateCandidate({email})

    const hashPassword = await bcrypt.hash(password, 3)
    const activationLink = uuid.v4()

    const user = await UserModel.create({
      _id: id,
      orgId,
      depId,
      email,
      roles: [ROLES.SERVICE_ROOT, ROLES.ORG_DIRECTOR],
      password: hashPassword,
      activationLink,
      fullName,
      status: STATUSES.INVITED,
      meta: {
        registerMethod: "password"
      }
    })

    await mailService.sendActivationMail(email, `${process.env.API_URL}/auth/activate/${activationLink}`);

    return AuthService.getUserAuthData(user)
  }

  async activate(activationLink) {
    const user = await UserModel.findOne({activationLink})
    if (!user) {
      throw ApiError.BadRequest("Некорректная ссылка активации")
    }

    user.activationLink = undefined
    user.meta.joinedAt = Date.now()
    user.status = STATUSES.ACTIVE
    await user.save()
  }

  async invitation(invitationLink) {
    const user = await UserModel.findOne({invitationLink})
    if (!user) {
      throw ApiError.BadRequest("Некорректная ссылка")
    }
    const inviteCode = uuid.v4()
    user.invitationLink = undefined
    user.inviteCode = inviteCode
    user.meta.joinedAt = Date.now()
    user.status = STATUSES.PRE_ACTIVE
    await user.save()

    return inviteCode
  }

  async invitationComplete(inviteCode, password) {
    const user = await UserModel.findOne({inviteCode})
    if (!user) {
      throw ApiError.BadRequest("Некорректная ссылка")
    }

    user.password = await bcrypt.hash(password, 3)
    user.inviteCode = undefined
    user.meta.joinedAt = Date.now()
    user.status = STATUSES.ACTIVE
    await user.save()

    this.updateVisitTime(user)
    return AuthService.getUserAuthData(user)
  }

  async login(email, password) {
    const user = await UserModel.findOne({email})
    if (!user || !user.password) {
      throw ApiError.BadRequest("Пользователь с такими данными не найден")
    }

    const isPassEquals = await bcrypt.compare(password, user.password)

    if (!isPassEquals) {
      throw ApiError.BadRequest("Пользователь с такими данными не найден")
    }

    this.updateVisitTime(user)

    return await AuthService.getUserAuthData(user)
  }

  async reset(email) {
    const user = await UserModel.findOne({email})
    if (!user) {
      throw ApiError.BadRequest("Пользователь с такими данными не найден")
    }

    const password = generate({
      length: 8,
      numbers: true
    })

    user.password = await bcrypt.hash(password, 3)
    await user.save()

    await mailService.sendResetPasswordMail(email, password)
  }


  async logout(refreshToken) {
    return await tokenService.removeToken(refreshToken)
  }

  async refresh(refreshToken) {
    if (!refreshToken) {
      throw ApiError.Unauthorized()
    }

    const userData = tokenService.validateRefreshToken(refreshToken)
    const tokenFromDb = await tokenService.findToken(refreshToken)

    if (!userData || !tokenFromDb) {
      throw ApiError.Unauthorized()
    }

    const user = await UserModel.findById(userData.id)

    this.updateVisitTime(user)

    return AuthService.getUserAuthData(user)
  }

  async updateVisitTime(user) {
    if (!user) return
    user.meta.lastVisitAt = Date.now()
    user.save()
  }

  static async getUserAuthData(user) {
    const userDto = new UserDto(user)
    const tokens = await AuthService.getUserTokens(userDto)
    return {...tokens, user: userDto}
  }

  static async getUserTokens(user) {
    const tokens = tokenService.generateTokens({...user})
    await tokenService.saveRefreshToken(user.id, tokens.refreshToken)
    return tokens
  }

}

module.exports = new AuthService()