module.exports.confirmEmail = function (link) {
  return `
        <div>
            <h1>Для завершения регистрации перейдите по ссылке</h1>
            <a href="${link}">${link}</a>
        </div>
      `
}

module.exports.inviteEmail = function (who, whom, link) {
  return `
        <div>
            <h1>Здраствуйте, <b>${whom}</b>!</h1>
            <h2>
                ${who} приглашает Вас присоедениться в приложение <b>${process.env.APP_NAME}</b>
            </h2>
            
            Чтобы присоедениться, перейдите по ссылке
            <a href="${link}">${link}</a>
        </div>
      `
}

module.exports.resetPasswordEmail = function (password) {
  return `
    <div>
        <h3>Ваш новый пароль:</h3>
        <b>${password}</b>
    </div>
  `
}