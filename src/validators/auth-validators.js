const {checkSchema} = require("express-validator");

class AuthValidators {
  email = checkSchema({
    email: {
      in: ['body'],
      trim: true,
      escape: true,
      toLowerCase: true,
      isEmail: {
        errorMessage: 'Некорректный адрес почты'
      }
    }
  })

  passwordMin = checkSchema({
    password: {
      in: ['body'],
      escape: true,
      notEmpty: true,
      isLength: {
        errorMessage: 'Пароль должен содержать не менее 6 символов',
        options: { min: 6 },
      },
    },
  })

  passwordMax = checkSchema({
    password: {
      in: ['body'],
      escape: true,
      isLength: {
        errorMessage: 'Пароль должен содержать не более 32 символов',
        options: { max: 32 },
      },
    },
  })
}


module.exports = new AuthValidators()