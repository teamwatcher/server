const {STATUSES} = require("../constants/user-constants");
module.exports.UserExportDto = class UserExportDto {
  "Email";
  "Полное ФИО";
  "Сокращенное ФИО";
  "Пол";
  "Статус";
  "Должность"
  "Команда"
  "Создан"
  "Последний визит"

  constructor(model) {
    this["Email"] = model.email
    this["Полное ФИО"] = model.fullName
    this["Сокращенное ФИО"] = model.displayName

    switch (model.gender) {
      case "male": this["Пол"] = "Муж."; break;
      case "female": this["Пол"] = "Жен."; break;
      default: this["Пол"] = "Не определено"
    }
    switch (model.status) {
      case STATUSES.CREATED: this["Статус"] = "Создан"; break;
      case STATUSES.INVITED: this["Статус"] = "Приглашен"; break;
      case STATUSES.PRE_ACTIVE: this["Статус"] = "Приглашение принято"; break;
      case STATUSES.ACTIVE: this["Статус"] = "Активен"; break;
      case STATUSES.DISABLED: this["Статус"] = "Заблокирован"; break;
    }

    this["Должность"] = model?.job?.position?.title
    this["Команда"] = model?.job?.team?.title

    this["Создан"] = model.meta.createdAt
    this["Последний визит"] = model.meta.lastVisitAt
  }

}