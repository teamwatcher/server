module.exports = class OrgDto {
  id;
  title;
  depId;

  constructor(model) {
    this.id = model._id
    this.depId = model.depId
    this.title = model.title
  }
}