module.exports = class UserDto {
  id;
  email;
  orgId;
  depId;
  status;
  roles;

  gender;
  fullName;
  displayName;

  job;
  invitedAt;


  constructor(model) {
    this.id = model._id
    this.email = model.email
    this.orgId = model.orgId
    this.depId = model.depId
    this.status = model.status
    this.roles = model.roles

    this.gender = model.gender
    this.fullName = model.fullName
    this.displayName = model.displayName

    this.job = Object.assign({}, model.job)
    this.invitedAt = model.meta.invitedAt

    this.planner = Object.assign({}, model.planner)
  }
}