module.exports.FileDto = class FileDto {
  id;
  name;
  original;
  meta;

  constructor(model) {
    this.id = model._id
    this.name = model.name
    this.original = model.original
    this.meta = model.meta
  }
}