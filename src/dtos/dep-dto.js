module.exports = class DepDto {
  id;
  orgId;
  ownerId;
  root;
  title;
  displayTitle
  files;
  emails;

  constructor(model) {
    this.id = model._id
    this.orgId = model.orgId
    this.parentId = model.parentId
    this.ownerId = model.ownerId
    this.root = model.root
    this.title = model.title
    this.displayTitle = model.displayTitle
    this.files = model.files
    this.emails = model.emails
  }
}