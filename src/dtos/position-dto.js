module.exports = class PositionDto {
  id;
  depId;
  title;
  displayTitle;

  constructor(model) {
    this.id = model._id
    this.depId = model.depId
    this.title = model.title
    this.displayTitle = model.displayTitle
  }
}