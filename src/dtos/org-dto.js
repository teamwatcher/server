const {FileDto} = require("./file-dto");
const UserDto = require("./user-dto");
const DepDto = require("./dep-dto");
module.exports = class OrgDto {
  id;
  ownerId;
  title;
  rootDep;

  emails;
  files;

  constructor(model) {
    this.id = model._id
    this.ownerId = model.ownerId
    this.title = model.title
    this.rootDep = new DepDto(model.rootDep)

    if (this.rootDep.ownerId) {
      this.rootDep.ownerId = new UserDto(model.rootDep.ownerId)
    }

    this.emails = model.emails
    this.files = model.files.map(file => new FileDto(file))
  }
}