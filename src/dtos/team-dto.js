module.exports = class TeamDto {
  id;
  depId
  ownerId;
  parentId;
  title;

  constructor(model) {
    this.id = model._id
    this.ownerId = model.ownerId
    this.depId = model.depId
    this.parentId = model.parentId
    this.title = model.title
  }
}