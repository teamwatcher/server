const {Schema, model} = require('mongoose')
const {meta} = require("./helper");

const EmailSchema = new Schema({
  orgId: {type: Schema.Types.ObjectId, ref: 'Struct/Org', required: true},
  name: {type: String, required: true},
  subject: {type: String, required: true},
  addManagers: {type: Boolean, default: true},
  addCC: {type: Array,},
  when: {type: Array,},
  body: {type: String, required: true},
  meta: meta()
})

module.exports.EmailModel = model('System/Email', EmailSchema)