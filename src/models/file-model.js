const {Schema, model} = require('mongoose')
const {meta} = require("./helper");
const {OrgModel} = require("./structure/org-model");

const FileSchema = new Schema({
  orgId: {type: Schema.Types.ObjectId, ref: 'Struct/Org', required: true},
  name: {type: String, required: true},
  original: {type: String, required: true},
  path: {type: String, required: true},
  meta: meta()
})

FileSchema.pre('save', {document: true}, async function (next) {
  await OrgModel.findOneAndUpdate({_id: this.orgId}, {$push: {files: this._id}})
  next()
})

module.exports.FileModel = model('System/File', FileSchema)