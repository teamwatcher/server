const {Schema, model} = require('mongoose')
const {UserModel} = require("./user-model");
const {meta} = require("../helper");

const PositionSchema = new Schema({
  orgId: {type: Schema.Types.ObjectId, ref: 'Struct/Org'},
  depId: {type: Schema.Types.ObjectId, ref: 'Dep'},
  title: {type: String, required: true},
  displayTitle: {type: String},
  meta: meta()
})


PositionSchema.pre('deleteOne', { document: true }, async function (next) {
  await UserModel.updateMany({'job.position': this._id}, {$unset: {'job.position': 1}})
  next()
})

module.exports.PositionModel = model('Struct/Position', PositionSchema)