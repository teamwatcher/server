const {Schema, model} = require('mongoose')
const {UserModel} = require("./user-model");
const {meta} = require("../helper");

const TaskSchema = new Schema({
  orgId: {type: Schema.Types.ObjectId, ref: 'Struct/Org'},
  depId: {type: Schema.Types.ObjectId, ref: 'Dep'},
  title: {type: String, required: true},
  meta: meta()
})

TaskSchema.pre('deleteOne', { document: true }, async function (next) {
  await UserModel.findOneAndUpdate(
    { 'job.taskList': { $elemMatch: { $eq: this._id } }},
    { $pull: {'job.taskList': this._id}}
  )
  next()
})

module.exports.TaskModel = model('Struct/Task', TaskSchema)