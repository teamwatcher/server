const {Schema, model} = require('mongoose')
const {ROLES} = require("../../constants/user-constants");
const {meta} = require("../helper");
const {UserModel} = require("./user-model");
const {TeamModel} = require("./team-model");
const {UserService} = require("../../service/structure/user-service");
const {TaskModel} = require("./task-model");
const {PositionModel} = require("./position-model");

const DepSchema = new Schema({
  orgId: {type: Schema.Types.ObjectId, ref: 'Struct/Org', required: true},
  parentId: {type: Schema.Types.ObjectId, ref: 'Struct/Dep'},
  ownerId: {type: Schema.Types.ObjectId, ref: 'Struct/User'},

  title: {type: String, required: true},
  displayTitle: {type: String, required: true},

  root: {type: Boolean, default: false},

  files:  [{type: Schema.Types.ObjectId, ref: 'System/File'}],
  emails: [{type: Schema.Types.ObjectId, ref: 'System/Email'}],
  meta: meta()
})

DepSchema.path('ownerId').set(function (val) {
  const oldOwner = this.ownerId?.toString()
  const newOwner = val?.toString()
  this._unsetRole = null
  this._setRole = null

  if (oldOwner !== newOwner) {
    if (oldOwner) this._unsetRole = oldOwner
    if (newOwner) this._setRole = newOwner
  }

  return val
})

DepSchema.pre('save',async function (next) {
  const role = this.root ? ROLES.ORG_DIRECTOR : ROLES.DEP_DIRECTOR

  if (this._unsetRole) await UserService.ROLE.unsetUserRole(DepModel, this._unsetRole, role)
  if (this._setRole) await UserService.ROLE.setUserRole(this._setRole, role, {
    depId: this._id,
    job: {
      team: undefined,
      taskList: [],
      position: undefined
    }
  })

  next()
})

DepSchema.pre('deleteOne', { document: true }, async function (next) {
  await UserModel.updateMany({'depId': this._id}, {$unset: {'depId': 1}})

  const teams = await TeamModel.find({orgId: this.orgId, depId: this._id})
  await Promise.all(teams.map(team => team.deleteOne()))

  const tasks = await TaskModel.find({orgId: this.orgId, depId: this._id})
  await Promise.all(tasks.map(task => task.deleteOne()))

  const positions = await PositionModel.find({orgId: this.orgId, depId: this._id})
  await Promise.all(positions.map(position => position.deleteOne()))

  if (this.ownerId) {
    const role = this.root ? ROLES.ORG_DIRECTOR : ROLES.DEP_DIRECTOR
    await UserService.ROLE.unsetUserRole(DepModel, this.ownerId, role)
  }

  next()
})

const DepModel = model('Struct/Dep', DepSchema)
module.exports.DepModel = DepModel