const {Schema, model} = require('mongoose')
const {meta} = require("../helper");

const OrgSchema = new Schema({
  ownerId: {type: Schema.Types.ObjectId, ref: 'Struct/User'},
  title: {type: String, required: true},
  rootDep: {type: Schema.Types.ObjectId, ref: 'Struct/Dep'},
  files: [{type: Schema.Types.ObjectId, ref: 'System/File'}],
  emails: [{type: Schema.Types.ObjectId, ref: 'System/Email'}],
  meta: meta()
})

module.exports.OrgModel = model('Struct/Org', OrgSchema)