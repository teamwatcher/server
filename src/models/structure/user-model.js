const {Schema, model} = require('mongoose')
const {STATUSES, ROLES} = require('../../constants/user-constants')
const {getDisplayNameAndGender} = require("../../helpers/utils");
const {meta} = require("../helper");

const UserSchema = new Schema({
  email: {type: String, unique: true, required: true},
  password: {type: String},

  orgId: {type: Schema.Types.ObjectId, ref: 'Struct/Org', required: true},
  depId: {type: Schema.Types.ObjectId, ref: 'Struct/Dep', required: true},

  status: {type: Number, default: STATUSES.CREATED},
  roles: {type: Array, default: [ROLES.DEP_USER]},

  activationLink: {type: String},
  invitationLink: {type: String},
  inviteCode: {type: String},

  fullName: {type: String, required: true},
  gender: {type: String},
  displayName: {type: String},

  job: {
    tabId: {type: String},
    taskList: [{type: Schema.Types.ObjectId, ref: 'Struct/Task'}],
    position: {type: Schema.Types.ObjectId, ref: 'Struct/Position'},
    team: {type: Schema.Types.ObjectId, ref: 'Struct/Team'},
  },

  training: {type: Boolean, default: false},
  meta: meta(true, true, {
    registerMethod: {type: String},

    invitedAt: {type: Date,},
    invitedBy: {type: Schema.Types.ObjectId, ref: 'Struct/User'},

    joinedAt: {type: Date},
    lastVisitAt: {type: Date},
  })
})

UserSchema.pre('deleteOne', {document: true}, function (next) {
  next()
})

UserSchema.pre('save', function (next) {
  if (this.isNew) {
    const {displayName, gender} = getDisplayNameAndGender(this.fullName)
    this.displayName = displayName
    this.gender = gender
  }

  next()
})

module.exports.UserModel = model('Struct/User', UserSchema)