const {Schema, model} = require('mongoose')
const {UserModel} = require("./user-model");
const {ROLES} = require("../../constants/user-constants");
const {UserService} = require("../../service/structure/user-service");
const {meta} = require("../helper");

const TeamSchema = new Schema({
  orgId: {type: Schema.Types.ObjectId, ref: 'Struct/Org'},
  depId: {type: Schema.Types.ObjectId, ref: 'Dep'},
  ownerId: {type: Schema.Types.ObjectId, ref: 'Struct/User'},
  parentId: {type: Schema.Types.ObjectId, ref: 'Struct/Team'},
  title: {type: String, required: true},
  meta: meta()
})

TeamSchema.path('ownerId').set(function (val) {
  const oldOwner = this.ownerId?.toString()
  const newOwner = val?.toString()
  this._unsetRole = null
  this._setRole = null

  if (oldOwner !== newOwner) {
    if (oldOwner) this._unsetRole = oldOwner
    if (newOwner) this._setRole = newOwner
  }

  return val
})

TeamSchema.pre('save', {document: true, query: false},async function (next) {
  if (this._unsetRole) await UserService.ROLE.unsetUserRole(TeamModel, this._unsetRole, ROLES.DEP_TEAM_LEADER)
  if (this._setRole) await UserService.ROLE.setUserRole(this._setRole, ROLES.DEP_TEAM_LEADER)

  next()
})

TeamSchema.pre('deleteOne',{ document: true }, async function (next) {
  await UserModel.updateMany({'job.team': this._id}, {$unset: {'job.team': 1}})
  await TeamModel.updateMany({parentId: this._id}, {$set: {parentId: this.parentId}})

  if (this.ownerId) {
    await UserService.ROLE.unsetUserRole(TeamModel, this.ownerId, ROLES.DEP_TEAM_LEADER)
  }

  next()
})

const TeamModel = model('Struct/Team', TeamSchema)
module.exports.TeamModel = TeamModel