const {Schema, model} = require('mongoose')

const TokenSchema = new Schema({
  user: {type: Schema.Types.ObjectId, ref: 'Struct/User'},
  refreshToken: {type: 'String', required: true},
})

module.exports = model('System/Token', TokenSchema)