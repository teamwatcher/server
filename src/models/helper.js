const {Schema} = require("mongoose");
module.exports.meta = function (created = true, modified = true, custom = {}) {
  const meta = {}
  if (created) {
    meta.createdAt = {type: Schema.Types.Date, default: new Date()}
    meta.createdBy = {type: Schema.Types.ObjectId, ref: 'Struct/User'}
  }

  if (modified) {
    meta.modifiedAt = {type: Schema.Types.Date}
    meta.modifiedBy = {type: Schema.Types.ObjectId, ref: 'Struct/User'}
  }

  Object.assign(meta, custom)
  return meta
}