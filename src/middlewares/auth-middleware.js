const ApiError = require('../exceptions/api-error')
const tokenService = require('../service/token-service')
const {ROLES} = require("../constants/user-constants");

module.exports = async function (req, res, next) {
  try {
    const authorizationHeader = req.headers.authorization

    if (!authorizationHeader) return next(ApiError.Unauthorized())

    const accessToken = authorizationHeader.split(' ')[1]
    if (!authorizationHeader) return next(ApiError.Unauthorized())

    const userData = tokenService.validateAccessToken(accessToken)
    if (!userData) return next(ApiError.Unauthorized())

    req.user = userData
    req.orgId = userData.orgId
    req.depId = userData.depId

    req.isService = userData.roles.includes(ROLES.SERVICE_ROOT) ||
        userData.roles.includes(ROLES.SERVICE_ADMIN)

    next()
  }catch (e) {
    return next(ApiError.Unauthorized())
  }
}