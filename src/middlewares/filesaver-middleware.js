const multer = require('multer');
const fs = require("fs");
const {extname} = require('path')

module.exports.FileSaverMiddleware = function (fileName, path = '') {
  const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      const {orgId} = req
      const absPath = `./uploads/org_${orgId}/${path}`
      fs.mkdirSync(absPath, { recursive: true })
      cb(null, absPath)
    },
    filename: function (req, file, cb) {
      cb(null, file.fieldname + extname(file.originalname))
    }
  })
  const upload = multer({ storage: storage });
  return upload.single(fileName)
}