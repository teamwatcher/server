const {validationResult} = require('express-validator')

module.exports = function (Error) {
  return function (req, res, next) {
    const errors = validationResult(req)

    if (!errors.isEmpty()) {
      Error.errors = errors.array()

      return next(Error)
    }

    next()
  }
}